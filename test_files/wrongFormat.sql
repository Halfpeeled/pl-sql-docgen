CREATE OR REPLACE PACKAGE_BODY "Test_Package" 
AS
	/**
 	 *@Purpose: This is a test package containing one useless function, and one perhaps even 
 	 *         more meaningless procedure. This package is for test purposes, and consequently 
 	 *         produces nil.  
	 */



	/**
  	 *@Changelog
	 *@Date: Never - Nobody - Did nothing to sate nobody.
	 *@Date: 20150712.1100 - Ethereal - Did jumping jacks, which produced no change in this meaningless code.
	 */
	
	TYPE test_rec IS TABLE OF NUMBER INDEX BY PLS_INTEGER; -- A table for nothing
	testGlobal NUMBER; -- A global variable for less than nothing

	/**
	 *@Name: testName1
	 *@Desc: This is test function number one. It takes one useless parameter and declares 
	 *       2 meaningless variables. It also prints a statement about the futility of this 
	 *       function's existence...
	 *@Param: The id of the made up test subject  
	 *@Return: Returns the imaginary v_test2
	 */
	FUNCTION test1
	(
		p_testID NUMBER
	)
	RETURN VARCHAR
	AS
		v_test1 NUMBER;
		v_test2 VARCHAR DEFAULT "test";
	BEGIN
		DBMS_OUTPUT.PUT_LINE("This is a test and was never meant to have any real function or meaning...");
		RETURN v_test2;
	EXCEPTION
	WHEN OTHERS THEN
		DBMS_OUTPUT.PUT_LINE(sqlerrm);
	END testName1;

	/**
	 *@Name: testName2
	 *@Desc: This is test2. It takes three parameters just for sake of doing it, it then assigns the to 
	 *       meaningless local variables
	 *@Param: The id of the made up test subject, the name of said subject, and one for funsies  
	 *@Return: Returns as void as the procedure itself is of meaning
	 */
	PROCEDURE test2
	(
		p_testID NUMBER,
		p_test_name VARCHAR,
		p_funsies BOOLEAN
	)
	AS
		v_test1 NUMBER;
		v_test2 VARCHAR DEFAULT "test";
		v_test3 BOOLEAN;
	BEGIN
		v_test1 := p_testID; 
		v_test2 := p_test_name;
		v_test3 := p_funsies;
	EXCEPTION
	WHEN OTHERS THEN
		DBMS_OUTPUT.PUT_LINE(sqlerrm);
	END testName2;
END Test_Package;