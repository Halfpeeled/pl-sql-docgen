package ccc_ait_util;

import java.util.ArrayList;

/**
 * Provides an object to store attributes pertaining to a Documentation File
 */
public class DocumentationFile
{
	private String name; // Package name
	private String purpose; // Package purpose
	private String changelog; // Package Changelog
	private ArrayList<String> variables; // Package level variables
	private ArrayList<CompleteBlock> cdb; // Complete declaration blocks (have headers) 
	private ArrayList<CodeBlock> odb; // Orphan declaration blocks (no headers)
	private ArrayList<CompleteBlock> cb; // Complete blocks (have headers)
	private ArrayList<CodeBlock> ob; // Orphan definition blocks (no header)
	private boolean isBody; // Is a body or spec
	private boolean packageEndError; // Had the specific error of not ending with the package name 
	private String outline; // Outline of the documentation file's contents

	/**
	 * Constructor
	 */
	public DocumentationFile()
	{
		variables = new ArrayList<String>();
		cdb = new ArrayList<CompleteBlock>();
		odb = new ArrayList<CodeBlock>();
		cb = new ArrayList<CompleteBlock>();
		ob = new ArrayList<CodeBlock>();
		packageEndError = false;
		outline = "";
	}

	public String getName()
	{
		return name;
	}

	public String getPurpose()
	{
		return purpose;
	}

	public String getChangelog()
	{
		return changelog;
	}

	public ArrayList<String> getVariables()
	{
		return variables;
	}

	public ArrayList<CompleteBlock> getCompleteDecBlocks()
	{
		return cdb;
	}

	public ArrayList<CodeBlock> getOrphanDecBlocks()
	{
		return odb;
	}

	public ArrayList<CompleteBlock> getCompleteBlocks()
	{
		return cb;
	}

	public ArrayList<CodeBlock> getOrphanBlocks()
	{
		return ob;
	}

	public boolean getIsBody()
	{
		return isBody;
	}

	public void setName(String n)
	{
		this.name = n;
	}

	public void setPurpose(String p)
	{
		this.purpose = p;
	}

	public void setChangelog(String c)
	{
		this.changelog = c;
	}

	public void setCompleteDecBlocks(ArrayList<CompleteBlock> cdb)
	{
		this.cdb.clear();
		for (int i = 0; i < cdb.size(); i++)
		{
			this.cdb.add(cdb.get(i));
		}
	}

	public void setOrphanDecBlocks(ArrayList<CodeBlock> odb)
	{
		this.odb.clear();
		for (int i = 0; i < odb.size(); i++)
		{
			this.odb.add(odb.get(i));
		}
	}

	public void setCompleteBlocks(ArrayList<CompleteBlock> cb)
	{
		this.cb.clear();
		for (int i = 0; i < cb.size(); i++)
		{
			this.cb.add(cb.get(i));
		}
	}

	public void setVariables(ArrayList<String> var)
	{
		this.variables.clear();
		for (int i = 0; i < var.size(); i++)
		{
			this.variables.add(var.get(i));
		}
	}

	public void setOrphanBlocks(ArrayList<CodeBlock> ob)
	{
		this.ob.clear();
		for (int i = 0; i < ob.size(); i++)
		{
			this.ob.add(ob.get(i));
		}
	}

	public void setBody(boolean ib)
	{
		this.isBody = ib;
	}

	public void setPackageEndError(boolean e)
	{
		this.packageEndError = e;
	}

	public boolean getPackageEndError()
	{
		return packageEndError;
	}

	/**
	 * Name: buildOutline
	 * Description: Builds an outline of the documentation file's contents
	 */
	public void buildOutline()
	{
		outline = "";

		// Populate the unordered list with the contents
		outline += (variables.size() > 0 ? "<b>Package Level Variables:</b><ul>" : "");
		for (int i = 0; i < variables.size(); i++)
		{
			outline += "<li>" + variables.get(i) + "</li>";
		}
		outline += (variables.size() > 0 ? "</ul>" : "");

		outline += "<b>Function and Procedures:</b><ul>";
		for (int i = 0; i < cdb.size(); i++)
		{
			outline += "<li><a href='#" + cdb.get(i).getBlock().getName() + "'>" + cdb.get(i).getBlock().getName()
					+ "</li>";
		}

		for (int i = 0; i < odb.size(); i++)
		{
			outline += "<li><a href='#" + odb.get(i).getName() + "'>" + odb.get(i).getName() + "</li>";
		}

		for (int i = 0; i < cb.size(); i++)
		{
			outline += "<li><a href='#" + cb.get(i).getBlock().getName() + "'>" + cb.get(i).getBlock().getName()
					+ "</a></li>";
		}

		for (int i = 0; i < ob.size(); i++)
		{
			outline += "<li><a href='#" + ob.get(i).getName() + "'>" + ob.get(i).getName() + "</li>";
		}
		outline += "</ul>";
	}

	/**
	 * Name: getOutline
	 * Description: Returns an outline of the documentation file's contents
	 * Return: HTML unordered list of outline as String
	 */
	public String getOutline()
	{
		return outline;
	}
}
