package ccc_ait_util;
/**
 * This class serves as an object for collecting Code Block elements (i.e. Name, Parameters, etc)
 * @author jmetcal6
 *
 */
public class CodeBlock
{
	private String code;
	private String type;
	private String name;
	private String[] params;
	private String returnType;
	
	// Procedure Constructor
	public CodeBlock(String code, String type, String name, String[] params)
	{
		this.code = code;
		this.type = type;
		this.name = name;
		this.params = params;
	}
	
	// Function Constructor
	public CodeBlock(String code, String type, String name, String[] params, String returnType)
	{
		this.code = code;
		this.type = type;
		this.name = name;
		this.params = params;
		this.returnType = returnType;
	}

	public String getCode()
	{
		return code;
	}
	
	public String getType()
	{
		return type;
	}
	
	public String getName()
	{
		return name;
	}

	public String[] getParams()
	{
		return params;
	}
	
	public String getReturnType()
	{
		return returnType;
	}
}
