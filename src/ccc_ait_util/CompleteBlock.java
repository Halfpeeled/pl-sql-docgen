package ccc_ait_util;

/**
 * Provides an object to store a block of code and its attributes, and it's accompanying anchor tag
 */
public class CompleteBlock
{
	private AnchorTag tag;
	private CodeBlock block;
	
	public CompleteBlock(AnchorTag tag, CodeBlock block)
	{
		this.tag = tag;
		this.block = block;
	}

	public AnchorTag getTag()
	{
		return tag;
	}

	public CodeBlock getBlock()
	{
		return block;
	}
}
