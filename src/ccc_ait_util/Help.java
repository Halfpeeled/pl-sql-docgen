package ccc_ait_util;

import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JEditorPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextPane;

/**
 * This class facilitates the help functionality
 *
 */
@SuppressWarnings("serial")
public class Help extends JPanel
{
	private JEditorPane helpPane; // Editor pane to display HTML formatted text
	private JScrollPane helpScroller; // Scroll pane for editor pane

	private JButton whatBtn;
	private JButton howBtn;
	private JButton detailedHowBtn;
	private JButton checklistBtn;
	private final String whatIsTxt = "<h2>What is the Documentation Generator?</h2><br>"
			+ "This Documentation Generator is designed to ease the documentation writing process for PL/SQL source code developed by CCC AIT. "
			+ "The Documentation Generator is similar to Javadocs, another tool used for Java code. By including specially formatted comments known as Anchor Tags within the source code, the application reads these "
			+ "and then formats them into more meaningful documentation that can then be transferred to external and/or centralized repositories. As it stands, the Documentation can only handle PL/SQL source code.";
	private final String howToTxt = "<h2>How to Use:</h2><br>"
			+ "To use the Documentation Generator, first your source code file must be <em>properly formatted</em> to ensure optimal results. Only standard non-proprietary file "
			+ "extensions are accepted by the application, such as: <font color='blue'>.txt, .sql</font'>. Proprietary file extensions such as <font color='red'>.doc, .pdf, and .pls</font> are not acceptable. Simply upload the formatted source code file you "
			+ "wish to generate documentation for, and the application will scan and extract the necessary information. This information is then formatted, displayed, and finally an accompanying <font color='blue'>.HTML</font> file is created and saved in the user specified directory. "
			+ "No changes are made to the original source code by the application. <center><h2>**Please read the Format Checklist before continuing**</h2></center>";
	private final String checklistTxt = "<h2>Source Code Formatting Readiness Checklist:</h2><br>"
			+ "<ol><li>Does you code compile? If the code contains syntax errors, the documentation will not be generated&nbsp; correctly. Please make sure "
			+ "the source code is syntactically correct by trying to compile it first, and correcting any errors if necessary.</li>"
			+ "<li>Does the source code file have the right file extension (<font color='blue'>.txt, .sql</font>)?</li>"
			+ "<li>Have you supplied the appropriate Anchor Tags? There are 4 main types of tags:</li>"
			+ "<ul><li>Purpose tag � used to detail the purpose of the package/source code</li>"
			+ "<li>Changelog tag � contains the individual changelogs and any associated JIRA issue links</li>"
			+ "<li>Variable Tag - used to detail package level variable</li>"
			+ "<li>Function/Procedure tags � used to detail the purpose and description of functions and procedures</li></ul>"
			+ "<li>Are your tags properly formatted? Each tag is formatted in a specific manner:"
			+ "<ol><li><font color='green'>/**<br>@PURPOSE<br>The purpose statement�<br>*/</font></li>"
			+ "<li><font color='green'>/**<br>@CHANGELOG<br>@DATE: 20160705.1600 - "
			+ "Joe Programmer - ISSUE(SDBAN-00) Some log text�<br>" + "*/</font></li>"
			+ "<li><font color='green'>/**<br>@VAR: Some made up variable for demo purposes.<br>*/</font></li>"
			+ "<li><font color='green'>/**<br>@NAME: someFunction<br>"
			+ "@DESC: Returns a value<br>@PARAM: None<br>@RETURN: a value as a VARCHAR2<br>*/</font></li>"
			+ "<center><h3>**For more detailed instructions on formatting and how the Documentation Generator works, refer to the Detailed Instructions section**</h3></center>";
	private final String detailedHowTxt = "<h2>How does it Work?</h2><br>"
			+ "The application searches the source code file you upload for specially formatted �Anchor Tags.� Each tag begins with <font color='green'>/**</font>, or a forward slash followed "
			+ "by two asterisks with NO spaces. Each tag ends with <font color='green'>*/</font>, or a single asterisk and a forward slash with NO spaces. The application determines the type of tag from the next element. This "
			+ "element will always be prefixed with the <font color='green'>@</font> symbol, i.e. <font color='green'>@Changelog</font>. Both the opening and closing sections of the tags MUST be on their own line.<br>"
			+ " The documentation generator handles four main types of Anchor Tags: Purpose tags (package headers/descriptions), Changelog tags, Variable tags, and Function and Procedure tags (Function and Procedure headers/descriptions).<br>"
			+ "<h2>The following are examples of each tag:</h2> "
			+ "<h3>PURPOSE TAG:</h3> For purpose tags, otherwise known as package headers/descriptions, start with the opening characters for any tag, then on the <em>next line</em> type <font color='green'>@PURPOSE. </font>"
			+ "On the <em>next line</em>, everything that follows until the closing tag is the purpose statement. Please refer to example below."
			+ "<h3>PURPOSE TAG EXAMPLE:</h3> <font color='green'>/**<br> @PURPOSE<br> This package doesn�t really exist, so there is no purpose� <br>*/"
			+ "</font> <h3>CHANGELOG TAG:</h3> For Changelog tags, start with the opening characters for any tag, then on the <em>next line</em> type <font color='green'>@CHANGELOG</font>. The individual logs start on the "
			+ "<em>next line</em> and are always started with <font color='green'>@DATE:</font> followed by the date in the appropriate format (<font color='green'>YYYYMMDD.24HR</font> i.e. <font color='green'>20160105.1700</font>)."
			+ " The date is then followed by a space, single hyphen, another space, and then the name of the programmer(s) writing the log. This is closed with another space, hyphen, and space. If there is a linked JIRA issue this is where " 
			+ "it would be supplied in the format of <font color='green'>ISSUE(the issue id)</font>. All text following is the actual log. "
			+ "The next log is determined by the next <font color='green'>@DATE:</font> encountered. Finally, close the tag with the appropriate characters <font color='green'>*/</font>. Please refer to the example below."
			+ " <h3>CHANGELOG TAG EXAMPLE:</h3> <font color='green'>/** <br> @CHANGELOG <br>@DATE: 20160105.1700 - Ada "
			+ "Lovelace � ISSUE(SDBAN-00) Log of changes made to code� <br>@DATE: 20160211.1100 � Von Neumann � Another log of some changes� <br>*/</font>"
			+ "<h3>VARIABLE TAG:</h3> For variable tags, start with the opening "
			+ "characters for any tag. On the <em>next line</em> type <font color='green'>@VAR:</font> followed by a space and then simply supply a brief description of the variable. Finally, close the tag with the appropriate characters <font color='green'>*/</font>. "
			+ "Please refer to example below."
			+ "<h3>VARIABLE EXAMPLE:</h3> <font color='green'>/**<br> @VAR: Non-existent variable description for demo purposes.<br>*/</font>"
			+ "<h3>FUNCTION/PROCEDURE TAG:</h3> For function/procedure tags, start with the opening "
			+ "characters for any tag. On the <em>next line</em> type <font color='green'>@NAME:</font> to start the first element of the tag, space, and then the name of the function/procedure. The second element should be <font color='green'>@DESC:</font> followed by the "
			+ "description of the function/procedure. The third element should be <font color='green'>@PARAM:</font> followed by any parameters of the function/procedure. The final element, which is optional to account for procedures, should be <font color='green'>@RETURN:</font> "
			+ "followed by the return value of the function. Finally, close the tag with the appropriate characters <font color='green'>*/</font>. Please refer to example below."
			+ "<h3>FUNCTION/PROCEDURE EXAMPLE:</h3> <font color='green'>/**<br> @NAME: someFunction <br>@DESC: A made up function that returns "
			+ "nothing� <br>@PARAM: None <br>@RETURN: a value that doesn�t really exist<br>*/</font>";

	// Constructor
	public Help()
	{
		this.setLayout(null);
		this.setBounds(5, 65, 685, 500);
		this.setBorder(DisplayPane.RAISED_LINE);
		this.setVisible(false);

		// Scroll pane for editor pane
		helpPane = new JTextPane(); // Text pane to display HTML formatted text
		helpScroller = new JScrollPane(helpPane, JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED, // Scroll pane for editor pane
				JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
		helpScroller.setBounds(5, 50, 675, 435);
		helpScroller.setBorder(DisplayPane.LOWERED_LINE);
		helpScroller.setViewportView(helpPane);
		helpPane.setContentType("text/html");
		helpPane.setMargin(new Insets(20, 20, 20, 20));
		helpPane.setEditable(false);
		showHelp(whatIsTxt);

		// Shows the What Is.. text button
		whatBtn = new JButton("About");
		whatBtn.setToolTipText("Displays the text explaining the purpose of this application");
		whatBtn.setBounds(5, 5, 135, 38);
		whatBtn.setBorder(DisplayPane.RAISED_LINE);
		whatBtn.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent e)
			{
				showHelp(whatIsTxt);
			}
		});

		// Shows the What Is.. text button
		howBtn = new JButton("How to Use");
		howBtn.setToolTipText("Displays the text explaining how to use this application");
		howBtn.setBounds(185, 5, 135, 38);
		howBtn.setBorder(DisplayPane.RAISED_LINE);
		howBtn.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent e)
			{
				showHelp(howToTxt);
			}
		});

		// Shows the What Is.. text button
		checklistBtn = new JButton("Format Checklist");
		checklistBtn.setToolTipText("Quick formatting checklist before trying to generate documentation");
		checklistBtn.setBounds(365, 5, 135, 38);
		checklistBtn.setBorder(DisplayPane.RAISED_LINE);
		checklistBtn.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent e)
			{
				showHelp(checklistTxt);
			}
		});

		// Shows the What Is.. text button
		detailedHowBtn = new JButton("Detailed Instructions");
		detailedHowBtn.setToolTipText("Detailed description of how the application works");
		detailedHowBtn.setBounds(545, 5, 135, 38);
		detailedHowBtn.setBorder(DisplayPane.RAISED_LINE);
		detailedHowBtn.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent e)
			{
				showHelp(detailedHowTxt);
			}
		});

		this.add(whatBtn);
		this.add(howBtn);
		this.add(checklistBtn);
		this.add(detailedHowBtn);
		this.add(helpScroller);
	}

	/**
	 * Name: showHelp
	 * Description: Displays the Documentation Generator's Help Text
	 */
	private void showHelp(String text)
	{
		String helpTxt = "<html><h1><center>Documentation Generator User Guide</center></h1><br>" + text + "</html>";
		helpPane.setText(helpTxt);
		helpPane.setCaretPosition(0);
	}
}
