package ccc_ait_util;

import java.awt.*;
import java.io.*;
import javax.swing.*;

/**
 * @author jmetcal6
 *	Class containing the heavy-weight frame and main method
 */
@SuppressWarnings("serial")
public class DocGen_Main extends JFrame
{
	// Constants for window size
	public static final int WIDTH = 700;
	public static final int HEIGHT = 600;
	private DisplayPane menu; // Instance of class for displaying graphical elements

	/**
	 * Constructor for GUI
	 * @throws IOException 
	 */
	public DocGen_Main() throws IOException
	{
		initComponents();
	}

	/**
	 * Overridden Constructor for console
	 * @throws IOException 
	 */
	public DocGen_Main(String path) throws IOException
	{
		menu = new DisplayPane(false);
	}

	/**
	 * Initializes the frame and it's children components 
	 * @throws IOException 
	 */
	private JFrame initComponents() throws IOException
	{
		// Frame
		JFrame f = new JFrame();
		Dimension screen = Toolkit.getDefaultToolkit().getScreenSize();
		int x = (screen.width - WIDTH) / 2;
		int y = (screen.height - HEIGHT) / 2;
		f.setBounds(x, y, WIDTH, HEIGHT);
		Image icon = Toolkit.getDefaultToolkit().createImage("img/docGenIcon.png");
		f.setIconImage(icon);
		f.setTitle("PL/SQL Documentation Generator");
		f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		f.setResizable(false);
		f.setLayout(null);

		// Child components 
		menu = new DisplayPane(true);
		menu.setBounds(0, 0, WIDTH, HEIGHT);

		f.add(menu);
		f.setVisible(true);
		return f;
	}

	/**
	 * Main
	 * @throws IOException 
	 */
	public static void main(String[] args) throws IOException
	{
		SwingUtilities.invokeLater(new Runnable()
		{
			public void run()
			{
				try
				{
					new DocGen_Main();
				}
				catch (IOException e)
				{
					e.printStackTrace();
				}
			}
		});
	}
}
