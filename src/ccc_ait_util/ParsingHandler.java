package ccc_ait_util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Class that handles all parsing, data extraction, and state changing operations
 * @author jmetcal6
 *
 */
public class ParsingHandler
{
	private ArrayList<DocumentationFile> docFiles; // Collection of documentation files and all of their members
	private String sourceFileName; // Variable to store filename
	private String changeLog; // Variable to store change log
	private String log; // Temporary variable to hold changelog
	private AnchorTag packageHeader; // Variable to store package header
	private String purpose = ""; // Temporary variable to hold packageHeader
	private ArrayList<AnchorTag> blockTags; // Array list to store formatted anchor tags 
	private ArrayList<String> variables; // Array list to store formatted anchor tags
	private String var; // Temporary container for formatting variable tag
	private String codeBlockTempTag; // Temporary container for function/procedure code blocks anchor tag
	private String codeBlockTemp; // Temporary container for function/procedure code blocks
	private ArrayList<String> rawCodeBlocks; // Collection of raw function/procedure code blocks
	private ArrayList<CodeBlock> formattedCodeBlocks; // Collection of formatted function/procedure code blocks
	private ArrayList<CompleteBlock> completeBlocks; // Collection of formatted function/procedure code blocks complete with anchor tags
	private ArrayList<CompleteBlock> completeDecBlocks; // Collection of formatted function/procedure declaration code blocks complete with anchor tags
	private ArrayList<CodeBlock> orphanDecBlocks; // Collection of formatted function/procedure declaration code blocks that had no preceding anchor tags
	private ArrayList<CodeBlock> orphanBlocks; // Collection of formatted function/procedure code blocks that had no preceding anchor tags
	private ArrayList<String> rawDecBlocks; // Collection of raw function/procedure declaration code blocks
	private ArrayList<CodeBlock> formattedDecBlocks; // Collection of formatted function/procedure declaration code
	private boolean precedingTag; // Boolean flag denoting whether a preceding header tag was encountered before a function or procedure block
	public final HashMap<String, String> keywords = new HashMap<String, String>(); // Keywords to parse for
	// Array of PLSQL Data Types
	private final String[] PLSQL_DATA_TYPES = { "binary,?;?", "dec,?;?", "decimal,?;?", "double,?;?", "float,?;?",
			"precision,?;?", "int,?;?", "integer,?;?", "natural,?;?", "naturaln,?;?", "number,?;?", "numeric,?;?",
			"pls_integer,?;?", "positive,?;?", "positiven,;??", "real,?;?", "signtype,?;?", "smallint,?;?", "char,?;?",
			"character,?;?", "long,?;?", "nchar,?;?", "nvarchar2,?;?", "raw,?;?", "rowid,?;?", "string,?;?",
			"urowid,?;?", "varchar,?;?", "varchar2,?;?", "boolean,?;?", "date,?;?", "interval,?;?", "timestamp,?;?",
			"record,?;?", "table,?;?", "varray,?;?", "cursor,?;?", "ref,?;?", "bfile,?;?", "blob,?;?", "clob,?;?",
			"nclob,?;?", "lob,?;?", "default,?;?", "in,?;?", "out,?;?", "null,?;?" };
	private String[] tokenizedDoc; // The tokenized form of characters in the file
	private final ParsingState parseState = new ParsingState(); // Instance of inner class that facilitates state checking and setting for parsing
	private boolean pkgEndErr; // Boolean flag for whether package ended with the package name

	/**
	 * Constructor
	 */
	public ParsingHandler()
	{
		docFiles = new ArrayList<DocumentationFile>();
		sourceFileName = "";
		blockTags = new ArrayList<AnchorTag>();
		variables = new ArrayList<String>();
		purpose = "";
		packageHeader = new AnchorTag();
		packageHeader.setPurpose("");
		log = "";
		changeLog = "";
		var = "";
		codeBlockTempTag = "";
		codeBlockTemp = "";
		rawCodeBlocks = new ArrayList<String>();
		rawDecBlocks = new ArrayList<String>();
		formattedCodeBlocks = new ArrayList<CodeBlock>();
		completeBlocks = new ArrayList<CompleteBlock>();
		orphanBlocks = new ArrayList<CodeBlock>();
		completeDecBlocks = new ArrayList<CompleteBlock>();
		orphanDecBlocks = new ArrayList<CodeBlock>();
		formattedDecBlocks = new ArrayList<CodeBlock>();
		precedingTag = false; // Boolean flag for whether a function or procedure was preceded by an anchor tag
		pkgEndErr = false;
		if (keywords.isEmpty()) // Initialize the keywords
		{
			keywords.put("WHITESPACE", "\\s+"); // Whitespace
			keywords.put("PURPOSE", "(?i:\\*?@purpose\\:?)"); // Purpose
			keywords.put("CHANGELOG", "(?i:\\*?@changelog\\:?)"); // Change log
			keywords.put("FUNC_OR_PROC", "(?i:\\*?@name\\:?|\\*?@desc\\:?|\\*?@param\\:?|\\*?@return\\:?)"); // @Name, @Desc, @Param, @Return
			keywords.put("VAR", "(?i:\\*?@var\\:?)"); // Variable
			keywords.put("END_LOOP", "(?i:loop;)"); // End Loop
			keywords.put("PROCEDURE", "procedure"); // Procedure
			keywords.put("FUNCTION", "function"); // Function
			keywords.put("BEGIN", "begin"); // Begin
			keywords.put("END", "end;?"); // End
			keywords.put("TAGBLOCK_START", "/\\*\\*$(?!/)"); // Anchor Tag Start
			keywords.put("TAGBLOCK_END", "\\*/"); // Anchor Tag End
			keywords.put("BLOCK_COMMENT_START", "/\\*"); // Block Comment Start
			keywords.put("SINGLE_LINE_BLOCK_COMMENT", "/\\*.*\\*/");
			keywords.put("LINE_COMMENT_START", "--"); // Line Comment Start
			keywords.put("END_IF", "(?i:if;)"); // End If
		}
	}

	/**
	 * Name: initStateChecking
	 * Description: Initializes the recursive state checking facilitated by inner class ParsingState
	 */
	public void initStateChecking(String tokens, int state, boolean isBody)
	{
		tokenizedDoc = tokens.split(keywords.get("WHITESPACE"));
		parseState.checkState(tokenizedDoc[0].toLowerCase(), state);
		docFiles.add(new DocumentationFile());
		docFiles.get(docFiles.size() - 1).setBody(isBody);
		docFiles.get(docFiles.size() - 1).setName(sourceFileName);
		docFiles.get(docFiles.size() - 1).setPurpose(getPackageHeader());
		docFiles.get(docFiles.size() - 1).setChangelog(getChangeLog());
		docFiles.get(docFiles.size() - 1).setVariables(variables);
		docFiles.get(docFiles.size() - 1).setCompleteDecBlocks(completeDecBlocks);
		docFiles.get(docFiles.size() - 1).setOrphanDecBlocks(orphanDecBlocks);
		docFiles.get(docFiles.size() - 1).setCompleteBlocks(completeBlocks);
		docFiles.get(docFiles.size() - 1).setOrphanBlocks(orphanBlocks);
		docFiles.get(docFiles.size() - 1).setPackageEndError(pkgEndErr);
		docFiles.get(docFiles.size() - 1).buildOutline();
		reset();
	}

	/**
	 * Name: organizeTags 
	 * Description: Parses the passed line of text for the
	 * specific anchors to append the appropriate values to the documentation
	 * file being built 
	 * Parameter: token as String
	 */
	public void parseTag(String token)
	{
		if (matchPattern(token, keywords.get("PURPOSE"))) // PURPOSE
		{
			extractPurpose();
		}
		else if (matchPattern(token, keywords.get("CHANGELOG"))) // Changelog
		{
			extractChangelog();
		}
		else if (matchPattern(token, keywords.get("FUNC_OR_PROC"))) // Any of the 4 elements contained only within function and procedure anchor tags
		{
			precedingTag = true; // Opening function/procedure header tag found so set flag
			codeBlockTempTag = ""; // Clear out temp container
			parseState.decrementTokenIndex(); // Decrement token index so the first pass does not skip the '@name' tag
			extractFuncProc();
		}
		else if (matchPattern(token, keywords.get("VAR"))) // Variable anchor tags
		{
			extractVariable();
		}

		// If the token is the end of the tag, return execution to checkState
		if (matchPattern(token, keywords.get("TAGBLOCK_END")))
		{
			parseState.decrementTokenIndex(); // Decrement token index to prevent double incrementing when execution returns to checkState
		}
		else
		{
			parseState.incrementTokenIndex(); // Advance the token index
			parseTag(tokenizedDoc[parseState.getTokenIndex()].toLowerCase()); // Keep parsing
		}
	}

	/**
	 * Name: extractPurpose
	 * Description: Walks through all tokens and concatenates them to the packageHeader container until the end of the tag is reached
	 */
	private void extractPurpose()
	{
		if (matchPattern(tokenizedDoc[parseState.getTokenIndex()], keywords.get("TAGBLOCK_END")))
		{
			parseState.decrementTokenIndex(); // Decrement token index to prevent double incrementing when execution returns to checkState
			packageHeader.setPurpose(purpose);
		}
		else
		{
			parseState.incrementTokenIndex(); // Advance the token index
			purpose += tokenizedDoc[parseState.getTokenIndex()].replaceAll("\\*", "").replaceAll("/", "") + " "; // Strip any comment characters
			extractPurpose(); // Recursively call until end of tag
		}
	}

	/**
	 * Name: extractChangelog
	 * Description: Walks through all tokens and concatenates them to the change log container until the end of the tag is reached
	 */
	private void extractChangelog()
	{
		if (matchPattern(tokenizedDoc[parseState.getTokenIndex()], keywords.get("TAGBLOCK_END")))
		{
			formatChangelog();
			parseState.decrementTokenIndex(); // Decrement token index to prevent double incrementing when execution returns to checkState
		}
		else
		{
			parseState.incrementTokenIndex(); // Advance the token index
			log += tokenizedDoc[parseState.getTokenIndex()].replaceAll("\\*", "").replaceAll("/", "") + " "; // Strip any comment characters
			extractChangelog(); // Recursively call until end of tag
		}
	}

	/**
	 * Name: formatChangelog
	 * Description: 
	 * Return: The formatted change log as a string
	 */
	private void formatChangelog()
	{
		String[] temp = log.split("@"); // Separate the change log entries by date
		changeLog += "<center><b><h2>Changelog</h2></b></center>";
		// Concatenate date entries to change log
		for (int i = 1; i < temp.length; i++) // Dates start after 2nd tokenIndex, as we split on @
		{
			changeLog += FileHandler.TABLE_HEAD + formatDate(temp[i]) + "</table><br>";
		}
	}

	/**
	 * Name: formatDate
	 * Description: Parse the passed change log entry for the date and formats it to a be more readable
	 * Parameters: The change log entry as a String
	 * Return: The formatted date appended to the change log entry
	 */
	private String formatDate(String e)
	{
		String formattedEntry = "";
		try
		{
			String[] entry = e.split("(?i:DATE:)");
			String date = entry[1].substring(0, entry[1].indexOf("-")); // Get the unformatted date
			String year = date.substring(1, 5); // Get the year
			String month = date.substring(5, 7); // Get the month
			String day = date.substring(7, 9); // Get the day
			String time = date.substring(date.indexOf(".") + 1, 14); // Get the time
			String programmer = "<b>Programmer(s): </b>" + entry[1]
					.substring(entry[1].indexOf("-") + 1, entry[1].indexOf("-", entry[1].indexOf("-") + 1)).trim(); // Get the programmer
			String log = "<b>Log:</b> "
					+ entry[1].substring(entry[1].indexOf("-", entry[1].indexOf("-") + 1) + 1).trim(); // Get the log

			// Check for linked issues/tickets within the log, if any are found, the log will be updated, otherwise it is left unchanged
			log = checkForLinkedIssue(log);

			// Remove leading zeros
			if (month.startsWith("0"))
			{
				month = month.replace("0", "");
			}
			if (day.startsWith("0"))
			{
				day = day.replace("0", "");
			}

			// Build date and wrap in table elements
			date = "<tr>" + FileHandler.TD_HEAD + "<b>Date:</b> " + month + "/" + day + "/" + year + " @"
					+ formatTime(time) + "</td></tr>";
			formattedEntry = date + "<tr>" + FileHandler.TD_HEAD + programmer + "</td></tr><tr>" + FileHandler.TD_HEAD
					+ log + "</td></tr>"; // Build log entry
		}
		catch (Exception e2)
		{
			formattedEntry = "<tr>" + FileHandler.TD_HEAD + "Date/Log improperly formatted</td></tr>"; // Build log entry
			FileHandler.logError("ERROR encountered in formatDate exception block. Date/Log inproperly formatted: "
					+ e2.getMessage());
		}

		return formattedEntry;
	}

	/**
	 * Name: formatTime
	 * Description: Helper function for formatDate that converts the 24hr time to 12hr format
	 * Parameters: The change log time as a String
	 * Return: The formatted time
	 */
	private String formatTime(String t)
	{
		try
		{
			final SimpleDateFormat sdf = new SimpleDateFormat("HHmm"); // 24hr
			final SimpleDateFormat sdf2 = new SimpleDateFormat("K:mm a"); // 12hr
			final Date date = sdf.parse(t);
			t = sdf2.format(date);
		}
		catch (final ParseException e)
		{
			FileHandler.logError("ERROR encountered in formatTime exception block: " + e.getMessage());
		}
		return t;
	}

	/**
	 * Name: checkForLinkedIssue
	 * Description: Checks the passed log entry for linked issue/ jira ticket tags, i.e. ISSUE(SDBAN-#), then builds a hyperlink out of it
	 * Parameter: The log entry as a String
	 * Return: The modified log entry with the link, or the unchanged log if no link was found
	 */
	private String checkForLinkedIssue(String log)
	{
		String issue = ""; // The extracted issue tag
		String link = ""; // The link built form the issue tag
		Pattern pat = Pattern.compile("ISSUE\\(.*\\)");
		Matcher matcher = pat.matcher(log);

		if (matcher.find())
		{
			issue = matcher.group();
			// Build the link out by prefixing the chemeketa JIRA directory and then remove the tag characters so only the issue id remains to complete the URL
			link = "<a href=\"https://projects.chemeketa.edu/browse/"
					+ issue.replace("ISSUE(", "").replace(")", "").trim() + "\">"
					+ issue.replace("ISSUE(", "").replace(")", "").trim() + "</a>";
			// Update the log by replacing the original linked issue with the built HyperLink
			log = log.replace(issue, link);
		}
		return log;
	}

	/**
	 * Name: extractVariable
	 * Description: Extracts the text for variable anchor tags
	 */
	private void extractVariable()
	{
		if (matchPattern(tokenizedDoc[parseState.getTokenIndex()], keywords.get("TAGBLOCK_END")))
		{
			variables.add(var);
			parseState.decrementTokenIndex(); // Decrement token index to prevent double incrementing when execution returns to checkState
		}
		else
		{
			parseState.incrementTokenIndex(); // Advance the token index
			var += tokenizedDoc[parseState.getTokenIndex()].replaceAll("\\*", "").replaceAll("/", "") + " "; // Strip any comment characters
			extractVariable(); // Recursively call until end of tag
		}
	}

	/**
	 * Name: extractFuncProc
	 * Description: Walks through all tokens and concatenates them to the temporary function or procedure container until the end of the tag is reached
	 */
	private void extractFuncProc()
	{
		if (matchPattern(tokenizedDoc[parseState.getTokenIndex()], keywords.get("TAGBLOCK_END")))
		{
			codeBlockTempTag = formatFuncOrProc();
			parseState.decrementTokenIndex(); // Decrement token index to prevent double incrementing when execution returns to checkState
		}
		else
		{
			parseState.incrementTokenIndex(); // Advance the token index
			codeBlockTempTag += tokenizedDoc[parseState.getTokenIndex()].replaceAll("\\*", "").replaceAll("/", "")
					+ " "; // Strip any comment characters
			extractFuncProc(); // Recursively call until end of tag
		}
	}

	/**
	 * Name: formatFuncOrProc
	 * Description: Formats a function/procedure tag block
	 * Return: The formatted function or procedure as a string
	 */
	private String formatFuncOrProc()
	{
		String type = "";
		String name = "";
		String desc = "";
		String param = "";
		String ret = "";
		String[] tagArry = codeBlockTempTag.split(keywords.get("WHITESPACE")); // Tokenize

		for (int i = 0; i < tagArry.length; i++)
		{
			if (tagArry[i].matches("(?i:@name\\:?)")) // @name
			{
				while (i < tagArry.length && !tagArry[i].matches("(?i:@desc\\:?|@param\\:?|@return\\:?)")) // Loop through until next tag is reached
				{
					name += tagArry[i] + " ";
					i++; // Increment outer loops index
				}
			}
			else if (tagArry[i].matches("(?i:@desc\\:?)"))
			{
				while (i < tagArry.length && !tagArry[i].matches("(?i:@name\\:?|@param\\:?|@return\\:?)"))
				{
					desc += tagArry[i] + " ";
					i++;
				}
			}
			else if (tagArry[i].matches("(?i:@param\\:?)"))
			{
				while (i < tagArry.length && !tagArry[i].matches("(?i:@name\\:?|@desc\\:?|@return\\:?)"))
				{
					param += tagArry[i] + " ";
					i++;
				}
			}
			else if (tagArry[i].matches("(?i:@return\\:?)"))
			{
				while (i < tagArry.length && !tagArry[i].matches("(?i:@name\\:?|@desc\\:?|@param\\:?)"))
				{
					ret += tagArry[i] + " ";
					i++;
				}
			}
			if (i < tagArry.length && tagArry[i].matches("(?i:@name\\:?|@desc\\:?|@param\\:?|@return\\:?)"))
			{
				i--; // Decrement index to prevent for loop from skipping over next tag 
			}
		}

		// Cleanup by removing tag names
		type = deriveTagType();
		name = name.replaceAll("(?i:@name\\:?)", "");
		desc = desc.replaceAll("(?i:@desc\\:?)", "");
		param = param.replaceAll("(?i:@param\\:?)", "");
		ret = ret.replaceAll("(?i:@return\\:?)", "");

		// Construct formatted anchor tag block and add to class level collection
		if (type.equalsIgnoreCase("procedure"))
		{
			blockTags.add(new AnchorTag(type, name, desc, param));
		}
		else
		{
			blockTags.add(new AnchorTag(type, name, desc, param, ret));
		}

		return codeBlockTempTag;
	}

	/**
	 * Name: deriveTagType Description: Returns the tag's type (function, etc)
	 * Parameter: The string block of code Return: String tag type
	 */
	private String deriveTagType()
	{
		String type = "No TYPE found";
		if (tokenizedDoc[parseState.getTokenIndex() + 1].equalsIgnoreCase("function"))
		{
			type = "FUNCTION";
		}
		else if (tokenizedDoc[parseState.getTokenIndex() + 1].toLowerCase().equals("procedure"))
		{
			type = "PROCEDURE";
		}

		return type;
	}

	/**
	 * Name: extractCode
	 * Description: Takes the most recently collected code block and extracts the Type, Name, Parameters, and Return (as necessary)
	 * Return: New CodeBlock object with extracted data
	 */
	private CodeBlock buildCodeBlock(ArrayList<String> code)
	{
		String[] extractedData = code.get(code.size() - 1).split(keywords.get("WHITESPACE")); // Always get the last element so it is the most current

		// If this is a function, construct with the return type
		if (extractedData[0].equalsIgnoreCase("function"))
		{
			return new CodeBlock(code.get(code.size() - 1), extractedData[0].toUpperCase(), checkName(extractedData[1]),
					checkParameters(extractedData[1], extractedData[2], code), returnType(extractedData));
		}
		// Otherwise construct as a procedure
		else
		{
			return new CodeBlock(code.get(code.size() - 1), extractedData[0].toUpperCase(), checkName(extractedData[1]),
					checkParameters(extractedData[1], (extractedData.length > 2 ? extractedData[2] : ""), code));
		}
	}

	/**
	 * Name: checkName
	 * Description: Returns the name of the function/procedure 
	 * Parameters: Token of code to be checked as String
	 * Return: The extracted name as String
	 */
	private String checkName(String token)
	{
		String name;

		// If there is parameters, the name is everything before them
		if (token.contains("("))
		{
			name = token.substring(0, token.indexOf("(")).trim();
		}
		// Otherwise it's just the token itself minus leading/trailing whitespace
		else
		{
			name = token.replace(";", "");
		}
		return name;
	}

	/**
	 * Name: checkParameters
	 * Description: Gets the last Function or Procedure block added to the collection and checks for parameters, and formats them for later use
	 * Return: Array of found parameters as String Array
	 */
	public String[] checkParameters(String token1, String token2, ArrayList<String> code)
	{
		String temp; // Temp String to facilitate substringing
		String params; // Temp String to facilitate substringing
		String[] paramTokens; // Array for  

		// Since each function or procedure starts with the Type and then the Identifier, the parentheses would be at the beginning of the third token/element
		if (token1.contains("(") || token2.contains("("))
		{
			// Get everything in between the parentheses
			temp = code.get(code.size() - 1).substring(code.get(code.size() - 1).indexOf("("));
			params = temp.substring(1, temp.indexOf(")"));
			// Split on whitespace to iterate token by token
			paramTokens = params.split(keywords.get("WHITESPACE"));

			// Walk through each token
			for (int i = 0; i < paramTokens.length; i++)
			{
				paramTokens[i] = formatPLSQLDataType(paramTokens[i]);
			}
			// Now call the helper method that rebuilds the parameter tokens into single line strings and return
			return rebuildParams(paramTokens);
		}
		// If no parenthese were found, then the function or procedure has no parameters
		else
		{
			return new String[] { "No Parameters" };
		}
	}

	/**
	 * Name: rebuildParams
	 * Description: Helper method for checkParameters that rebuilds the parameter tokens into single line Strings for easier formatting
	 * Parameters: parameters tokens as String Array
	 * Return: The single line parameters as String Array
	 */
	private String[] rebuildParams(String[] params)
	{
		String temp = "";
		String[] rebuiltParams;

		for (int i = 0; i < params.length; i++)
		{
			temp += params[i] + " ";
		}
		if (params.length > 1)
		{
			rebuiltParams = temp.split(",");
			return rebuiltParams;
		}
		return params;
	}

	/**
	 * Name: returnType
	 * Description: Extracts the return type from the passed code block
	 * Parameter: block of code to be checked as String Array
	 * Return: Return type as String
	 */
	private String returnType(String[] data)
	{
		String ret = "";
		for (int i = 0; i < data.length; i++)
		{
			if (data[i].equalsIgnoreCase("RETURN"))
			{
				ret = data[i + 1];
				break;
			}
		}
		return formatPLSQLDataType(ret);
	}

	/**
	 * Name: matchPattern
	 * Description: Takes a given String and a regex pattern and determines whether pattern is within the supplied String
	 * Parameters: The token to be checked as String, The regex pattern as String
	 * Return: True or False dependent on whether pattern is found
	 */
	public boolean matchPattern(String token, String regex)
	{
		Pattern pat = Pattern.compile(regex);
		Matcher matcher = pat.matcher(token);

		if (matcher.find())
		{
			return true;
		}
		else
		{
			return false;
		}
	}

	/**
	 * Name: formatPLSQLDataType
	 * Description: Checks to see if the passed token matches any of PLSQL's datatypes. If it does, formats it to be all uppercase
	 * Parameter: Passed token to be checked as String
	 * Return: The formatted token as String
	 */
	private String formatPLSQLDataType(String token)
	{
		// Walk through array of PLSQL data types
		for (int i = 0; i < PLSQL_DATA_TYPES.length; i++)
		{
			// If there is a match, replace the token with an uppercase version of itself
			if (token.toLowerCase().matches(PLSQL_DATA_TYPES[i]))
			{
				token = token.toUpperCase();
			}
		}
		return token;
	}

	/**
	 * Name: isDeclaration
	 * Description: Determines if the function or procedure is a declaration or a definition
	 * Return: isDec as boolean
	 */
	public boolean isDeclaration()
	{
		boolean isDec = true;
		String temp = ""; // Temporary String container
		int index = parseState.getTokenIndex(); // Local index to increment based off of the current parsing index
		String[] tempArry; // Temporary String array container
		// Loop until first instance of terminator ';'. For declarations, this is the end of the declaration, for definitions it is at minimum past The IS/AS keyword and before the end of the block.
		while (!matchPattern(tokenizedDoc[index].toLowerCase(), ";"))
		{
			temp += tokenizedDoc[index] + " ";
			index++;
		}

		// Tokenize the string
		tempArry = temp.split(keywords.get("WHITESPACE"));
		for (int i = 0; i < tempArry.length; i++)
		{
			// Check for AS/IS keywords
			if (matchPattern(tempArry[i], "(?i:\\bis\\b)") || (matchPattern(tempArry[i], "(?i:\\bas\\b)")))
			{
				isDec = false;
			}
		}

		// If AS/IS wasn't found, this is a declaration, so collect appropriately
		if (isDec)
		{
			temp += tokenizedDoc[index]; // Get the last token
			buildDecBlock(temp);
		}

		return isDec;
	}

	/**
	 * Name: buildDecBlock
	 * Description: Formats and builds the declaration block and adds to the appropriate collection
	 * Parameter: The code block to be formatted as string
	 */
	private void buildDecBlock(String code)
	{
		rawDecBlocks.add(code);
		// If the current function or procedure block had a preceding anchor tag
		if (precedingTag)
		{
			formattedDecBlocks.add(buildCodeBlock(rawDecBlocks)); // Add block of code and extracted and formatted parameters to collection of formatted code blocks
			completeDecBlocks.add(new CompleteBlock(blockTags.get(blockTags.size() - 1),
					formattedDecBlocks.get(formattedDecBlocks.size() - 1))); // Add last anchor tag and formatted code block to collection of complete code blocks
		}
		else
		{
			orphanDecBlocks.add(buildCodeBlock(rawDecBlocks)); // Add block of formatted code that has no preceding tag to collection
		}

		precedingTag = false; // Since this is the end of the function or procedure block, reset the boolean flag
	}

	/**
	 * Accessors and Mutators for all variables that need accessed by FileHandler
	 * 
	 */
	public void setSourceFileName(String name)
	{
		sourceFileName = name;
	}

	public HashMap<String, String> getKeywords()
	{
		return keywords;
	}

	public String getSourceFileName()
	{
		return sourceFileName;
	}

	public DocumentationFile getDocFile()
	{
		return docFiles.get(docFiles.size() - 1);
	}

	public String getChangeLog()
	{
		return changeLog;
	}

	public String getPackageHeader()
	{
		return packageHeader.getPurpose();
	}

	/**
	 * Name: reset
	 * Resets all variables
	 */
	public void reset()
	{
		sourceFileName = "";
		purpose = "";
		log = "";
		variables.clear();
		packageHeader.resetPurpose();
		changeLog = "";
		codeBlockTempTag = "";
		codeBlockTemp = "";
		blockTags.clear();
		formattedCodeBlocks.clear();
		rawCodeBlocks.clear();
		orphanBlocks.clear();
		completeBlocks.clear();
		orphanDecBlocks.clear();
		completeDecBlocks.clear();
		rawDecBlocks.clear();
		formattedDecBlocks.clear();
		precedingTag = false;
		parseState.reset();
		pkgEndErr = false;
	}

	/**
	 * 
	 * Inner class to facilitate state handling
	 *
	 */
	public class ParsingState
	{
		/* State Legend - Map for corresponding number values of possible states
		 * 0 = Normal
		 * 1 = BEGIN
		 * 2 = END
		 * 3 = PROCEDURE
		 * 4 = FUNCTION
		 * 5 = Tag start
		 * 6 = Tag end
		 */

		public static final int IRRELEVANT = 0; // Entering this state is irrelevant
		public static final int CORRECT = 1; // Entering this state is correct
		public static final int VIOLATION = 2; // Entering this state is a violation

		// 2D array of states and rules determining how much of a scrub I am
		public final int[][] statePath = new int[][] {
				{ CORRECT, CORRECT, VIOLATION, CORRECT, CORRECT, CORRECT, VIOLATION }, // Normal/Default state
				{ IRRELEVANT, CORRECT, CORRECT, VIOLATION, VIOLATION, CORRECT, VIOLATION }, // BEGIN
				{ CORRECT, CORRECT, CORRECT, VIOLATION, VIOLATION, CORRECT, VIOLATION }, // END  
				{ IRRELEVANT, CORRECT, VIOLATION, CORRECT, CORRECT, VIOLATION, VIOLATION }, // PROCEDURE
				{ IRRELEVANT, CORRECT, VIOLATION, CORRECT, CORRECT, VIOLATION, VIOLATION }, // FUNCTION
				{ IRRELEVANT, IRRELEVANT, IRRELEVANT, IRRELEVANT, IRRELEVANT, IRRELEVANT, CORRECT }, // TAG_START
				{ CORRECT, CORRECT, CORRECT, CORRECT, CORRECT, CORRECT, VIOLATION } }; // TAG_END

		// Constants for meaningful names to numeric state values		
		private final int NORMAL_STATE = 0;
		private final int BEGIN_STATE = 1;
		private final int END_STATE = 2;
		private final int PROCEDURE_STATE = 3;
		private final int FUNCTION_STATE = 4;
		private final int TAG_START_STATE = 5;
		private final int TAG_END_STATE = 6;
		private int state; // Current parsing state 
		private int lastState; // Set state based on last occurrence of function or procedure

		private int unbalancedBegin; // Count of BEGIN keywords encountered while parsing. Incremented when BEGIN is encountered, decremented by matching END
		private int unbalancedProcFunc; // Count of PROCEDURE or FUNCTION keywords encountered while scrubbing/parsing. Incremented when either is encountered, decremented by matching END
		private int tokenIndex; // The index of current token from parent class-level collection
		private boolean ignore; // Boolean flagging whether tokens are contained within block comment and should be ignored 
		private boolean caseStmt; // Boolean flagging if parser is inside of case statement so it knows to ignore the closing END keyword

		// Constructor
		public ParsingState()
		{
			state = NORMAL_STATE;
			lastState = 0;
			tokenIndex = 0;
			unbalancedBegin = 0;
			unbalancedProcFunc = 0;
			ignore = false;
			caseStmt = false;
		}

		/**
		 * Name: checkState
		 * Description: Checks the current token and previous state and calls the helper method setState to set the appropriate state, 
		 * then advances via a recursive call
		 * Parameters: token, prevState
		 */
		public void checkState(String token, int prevState)
		{
			// If a valid state
			if (prevState >= 0 && prevState <= 6)
			{
				// Set the state via helper method
				setState(token, prevState);
			}
			else
			{
				FileHandler.logError(
						"ERROR: CRITICAL ERROR! Invalid/undefined state reached. Valid states are in the numeric range of 0-6. This shouldn't be possible.");
			}

			tokenIndex++; // Advance to the next token in the collection

			// If the index is less the token collection's length
			if (tokenIndex < tokenizedDoc.length)
			{
				checkState(tokenizedDoc[tokenIndex].toLowerCase(), state);
			}
		}

		/**
		 * Name: setState
		 * Description: Helper method for checkState that sets the current state via checking the current token and previous state.
		 * Parameters: token, prevState
		 */
		private void setState(String token, int prevState)
		{
			/*###################################################################################################
			 *  BEGIN
			 */
			if (token.equals(keywords.get("BEGIN")) && !ignore) // BEGIN
			{
				if (statePath[prevState][1] == CORRECT) // Valid state
				{
					unbalancedBegin++; // Increment unbalanced begin count
					state = BEGIN_STATE; // Set state
				}
				else if (statePath[prevState][1] == VIOLATION) // Invalid state
				{
					FileHandler.logError(
							"ERROR: Keyword BEGIN without closing END encountered. This is a syntax error that needs to be corrected in the source code. @ Token Index: "
									+ tokenIndex);
				}
			}
			/*###################################################################################################
			 *  END
			 *  Additional conditions check if next token is End of IF or LOOP to prevent erroneously changing states due to the closing of IF or Iterative blocks
			 */
			else if (token.matches(keywords.get("END")) && !isLoopOrConditional() && !ignore) // END
			{
				if (statePath[prevState][2] == CORRECT && statePath[prevState][1] == CORRECT && !caseStmt) // Valid state
				{
					unbalancedBegin--; // Decrement unbalanced BEGIN count every time END is arrived at correctly
					state = END_STATE; // By default set state to END

					// If BEGINs are now 0, that means a nested function / procedure has closed, so set state to previous function / procedure state
					if (unbalancedBegin == 0)
					{
						unbalancedProcFunc--; // Decrement unbalanced procedure/function count
						state = lastState; // Set state to originating function / procedure state
						lastState = 0; // Clear out last function / procedure state
					}
					// Otherwise the parent block is closed, so return to normal state
					if (unbalancedBegin == 0 && unbalancedProcFunc == 0)
					{
						state = NORMAL_STATE;
						extractCode(); // Extract and build the code block and add to collection
					}
				}
				else if (statePath[prevState][2] == VIOLATION) // Invalid state
				{
					// If NOT an error because the end of the file has been reached (Final END that closes the package that has no preceding BEGIN)
					if (endOfFile() || matchPattern(tokenizedDoc[tokenIndex + 1].toLowerCase(),
							"[\"?" + sourceFileName.toLowerCase() + ";?\"?]"))
					{
						/* 
						   If this is the end of the file because it is the last token, 
						   and the package does NOT end with the package name, then set the pkgEndErr to true
						   to notify the user later.
						*/
						if (endOfFile())
						{
							pkgEndErr = true;
						}
					}
					else // Otherwise it really is an invalid state
					{
						FileHandler.logError(
								"ERROR: Keyword END without matching parent BEGIN encountered. This is a syntax error that needs to be corrected in the source code. @ Token Index: "
										+ tokenIndex);
					}
				}
				// Since the only ending condition of a case statement is END, set false after every END is encountered
				if (caseStmt)
				{
					caseStmt = false;
				}
			}
			/*###################################################################################################
			 *  PROCEDURE
			 */
			else if (token.equals(keywords.get("PROCEDURE")) && !ignore && !isDeclaration()) // PROCEDURE
			{
				if (statePath[prevState][3] == CORRECT) // Valid state
				{
					unbalancedProcFunc++; // Increment unbalanced procedure/function count
					state = PROCEDURE_STATE; // Set state
					lastState = PROCEDURE_STATE; // Set the originating procedure state
				}
				else if (statePath[prevState][3] == VIOLATION) // Invalid state
				{
					FileHandler.logError(
							"ERROR: Keyword PROCEDURE encountered in inappropriate area. This is a syntax error that needs to be corrected in the source code. @ Token Index: "
									+ tokenIndex);
				}
			}
			/*###################################################################################################
			 *  FUNCTION
			 */
			else if (token.equals(keywords.get("FUNCTION")) && !ignore && !isDeclaration()) // FUNCTION
			{
				if (statePath[prevState][4] == CORRECT) // Valid state
				{
					unbalancedProcFunc++; // Increment unbalanced procedure/function count
					state = FUNCTION_STATE; // Set state
					lastState = FUNCTION_STATE; // Set the originating function state
				}
				else if (statePath[prevState][4] == VIOLATION) // Invalid state
				{
					FileHandler.logError(
							"ERROR: Keyword FUNCTION encountered in inappropriate area. This is a syntax error that needs to be corrected in the source code. @ Token Index: "
									+ tokenIndex);
				}
			}
			/*###################################################################################################
			 *  TAGBLOCK START
			 */
			else if (matchPattern(token, keywords.get("TAGBLOCK_START"))
					&& !matchPattern(token, keywords.get("SINGLE_LINE_BLOCK_COMMENT"))) // TAG START
			{
				if (statePath[prevState][5] == CORRECT) // Valid state
				{
					ignore = true; // Set flag so an keywords encountered within tags are ignored
					state = TAG_START_STATE; // Set state
					parseTag(token); // Parse the tag to determine it's type, and format and collect appropriately
				}
				// Since tag blocks can technically begin anywhere, syntax errors are impossible 
			}
			/*###################################################################################################
			 *  TAGBLOCK / BLOCK COMMENT END
			 */
			else if (matchPattern(token, keywords.get("TAGBLOCK_END"))) // TAG END
			{
				if (statePath[prevState][6] == CORRECT) // Valid state
				{
					state = TAG_END_STATE; // Set state
				}
				else if (statePath[prevState][6] == VIOLATION
						&& !matchPattern(token, keywords.get("SINGLE_LINE_BLOCK_COMMENT")) && !ignore) // Invalid state
				{
					FileHandler.logError(
							"ERROR: TAGBLOCK END characters ( */ ) encountered without preceding TAGBLOCK START ( /** ) characters. This is a syntax error that needs to be corrected in the source code. @ Token Index: "
									+ tokenIndex);
				}
				ignore = false; // Block comment or tag has ended, so stop ignoring keywords. This must be set AFTER state checking conditionals to prevent false flag error logs
			}
			/*###################################################################################################
			 *  BLOCK COMMENT START
			 */
			else if (matchPattern(token, keywords.get("BLOCK_COMMENT_START"))
					&& !matchPattern(token, keywords.get("SINGLE_LINE_BLOCK_COMMENT"))) // Block Comment Start
			{
				ignore = true; // Set flag so any keywords encountered within block comments are ignored
			}
			/*###################################################################################################
			 *  CASE Statement (Closed with END keyword, so needs to be flagged to avoid erroneous state changing)
			 */
			else if (token.equalsIgnoreCase("case") && !ignore)
			{
				caseStmt = true;
			}

			/* If any keywords representing a block of code are encountered, concatenate the token to the temporary container
			When all Function/Procedure and Begin/End keywords are balanced (the block has been closed), then it will be 
			cleared for the next one (in the condition above that checks for END state)*/
			if (isfuncProcState() || state == BEGIN_STATE || state == END_STATE)
			{
				codeBlockTemp += token + " ";
			}
		}

		/**
		 * Name: endOfFile
		 * Description: Determines if the end of the file has been reached by checking if the current token index is still less the the tokenized doc length (an array)
		 * Return: True or false depending on if the end of the file was reached
		 */
		private boolean endOfFile()
		{
			if (tokenIndex + 1 <= tokenizedDoc.length - 1)
			{
				return false;
			}
			else
			{
				return true;
			}
		}

		/**
		 * Name: isLoopOrConditional
		 * Description: First checks to see if the end of the fiel has been reached, and if not, checks if the next token is loop or if, 
		 * which would mean the previous END keyword was just closing a conditional or iterative block
		 * Return: True or false depending on whether if or loop was found. Automatically returns false if it's the end of the file 
		 * @return
		 */
		private boolean isLoopOrConditional()
		{
			// If not the end of the file
			if (!endOfFile())
			{
				// If the next token matches if or loop
				if (!matchPattern(tokenizedDoc[tokenIndex + 1], keywords.get("END_IF"))
						&& !matchPattern(tokenizedDoc[tokenIndex + 1], keywords.get("END_LOOP")))
				{
					return false;
				}
				else
				{
					return true;
				}
			}
			// Otherwise return false by default because there is not next token to check
			else
			{
				return false;
			}
		}

		/**
		 * Name: extractCode
		 * Description: Determines if the block of code had a preceding tag, and adds to to appropriate collection after formatting via buildCodeBlock()
		 */
		private void extractCode()
		{
			codeBlockTemp += tokenizedDoc[tokenIndex] + " " + tokenizedDoc[tokenIndex + 1]; // Grab the end of the block since state will return to normal						
			rawCodeBlocks.add(codeBlockTemp); // Add to the collection of raw code blocks
			codeBlockTemp = ""; // Clear out temp container

			// If the current function or procedure block had a preceding anchor tag
			if (precedingTag)
			{
				formattedCodeBlocks.add(buildCodeBlock(rawCodeBlocks)); // Add block of code and extracted and formatted parameters to collection of formatted code blocks
				completeBlocks.add(new CompleteBlock(blockTags.get(blockTags.size() - 1),
						formattedCodeBlocks.get(formattedCodeBlocks.size() - 1))); // Add last anchor tag and formatted code block to collection of complete code blocks
			}
			else
			{
				orphanBlocks.add(buildCodeBlock(rawCodeBlocks)); // Add block of formatted code that has no preceding tag to collection
			}

			precedingTag = false; // Since this is the end of the function or procedure block, reset the boolean flag
		}

		/**
		 * Name: isfuncProcState
		 * Description: Determines if the current state is function or procedure
		 * Return: true or false if state is a function or procedure sate
		 */
		public boolean isfuncProcState()
		{
			if (state == FUNCTION_STATE || state == PROCEDURE_STATE)
			{
				return true;
			}
			else
			{
				return false;
			}
		}

		/**
		 * Name: getState
		 * Description: Returns the current parsing state
		 */
		public int getState()
		{
			return state;
		}

		/**
		 * Name: getTokenIndex
		 * Description: Returns the current token index
		 */
		public int getTokenIndex()
		{
			return tokenIndex;
		}

		/**
		 * Name: setTokenIndex
		 * Description: Sets the current token index
		 */
		public void setTokenIndex(int index)
		{
			tokenIndex = index;
		}

		/**
		 * Name: incrementTokenIndex
		 * Description: Increments tokenIndex by 1
		 */
		public void incrementTokenIndex()
		{
			tokenIndex++;
		}

		/**
		 * Name: decrementTokenIndex
		 * Description: Decrements tokenIndex by 1
		 */
		public void decrementTokenIndex()
		{
			tokenIndex--;
		}

		/**
		 * Name: reset
		 * Description: Resets all variables
		 */
		public void reset()
		{
			state = NORMAL_STATE;
			lastState = 0;
			tokenIndex = 0;
			unbalancedBegin = 0;
			unbalancedProcFunc = 0;
			ignore = false;
			caseStmt = false;
			pkgEndErr = false; 
		}
	}
}
