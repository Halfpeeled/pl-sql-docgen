package ccc_ait_util;

import java.io.File;

/**
 * Provides an object to store attributes pertaining to a Source Code File
 */
public class SourceCodeFile
{
	private File file;
	
	public SourceCodeFile(File file)
	{
		this.file = file;
	}

	public File getFile()
	{
		return file;
	}

	public void setFile(File file)
	{
		this.file = file;
	}

	public String getSourceName()
	{
		return file.getName();
	}
}
