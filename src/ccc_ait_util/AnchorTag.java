package ccc_ait_util;

import java.util.ArrayList;

/**
 * Class for creating objects to store Anchor Tag elements
 */
public class AnchorTag
{
	private String tagType;
	private String tagName;
	private String tagDesc;
	private String tagParam;
	private String tagReturn;
	private ArrayList<String> actualParams;
	private String purpose;
	private ArrayList<String> logs = new ArrayList<String>();

	/** 
	 * Constructor for Package Purpose and Changelog Anchor Tags
	 */
	public AnchorTag()
	{
		
	}

	/**
	 * Function Constructor
	 */
	public AnchorTag(String tagType, String tagName, String tagDesc, String tagParam, String tagReturn)
	{
		actualParams = new ArrayList<String>();
		this.tagType = tagType;

		// Checks desc, param, and return tags for values, if null/empty say so
		if (tagName.trim().isEmpty())
		{
			this.tagName = "<b>Name:</b> No name provided within header";
		}
		else
		{
			this.tagName = tagName;
		}
		// Checks desc, param, and return tags for values, if null say so
		if (tagDesc.trim().isEmpty())
		{
			this.tagDesc = "No description provided within header";
		}
		else
		{
			this.tagDesc = tagDesc;
		}
		if (tagParam.trim().isEmpty())
		{
			this.tagParam = "No Parameters provided within header";
		}
		else
		{
			this.tagParam = tagParam;
		}
		if (tagReturn.trim().isEmpty())
		{
			this.tagReturn = "No return provided within header or N/A";
		}
		else
		{
			this.tagReturn = tagReturn;
		}
	}

	/**
	 * Procedure Constructor
	 */
	public AnchorTag(String tagType, String tagName, String tagDesc, String tagParam)
	{
		actualParams = new ArrayList<String>();
		this.tagType = tagType;

		// Checks desc, param, and return tags for values, if null/empty say so
		if (tagName.trim().isEmpty())
		{
			this.tagName = "<b>Name:</b> No name provided within header";
		}
		else
		{
			this.tagName = tagName;
		}
		// Checks desc, param, and return tags for values, if null say so
		if (tagDesc.trim().isEmpty())
		{
			this.tagDesc = "No description provided within header";
		}
		else
		{
			this.tagDesc = tagDesc;
		}
		if (tagParam.trim().isEmpty())
		{
			this.tagParam = "No Parameters provided within header";
		}
		else
		{
			this.tagParam = tagParam;
		}
		this.tagReturn = null;
	}
	
	/**
	 * Name: resetChangeLog 
	 * Description: Resets all Changelog Tag variables
	 */
	public void resetChangeLog()
	{
		logs.clear();
	}
	
	/**
	 * Name: resetPurpose 
	 * Description: Resets all Purpose Tag variables
	 */
	public void resetPurpose()
	{
		purpose = "";
	}
	
	/**
	 * Name: resetFunction 
	 * Description: Resets all Function Tag variables
	 */
	public void resetFunction()
	{
		tagType = "";
		tagName = "";
		tagDesc = "";
		tagParam = "";
		tagReturn = "";
		actualParams.clear();
	}
	
	/**
	 * Name: resetProcedure 
	 * Description: Resets all Procedure Tag variables
	 */
	public void resetProcedure()
	{
		tagType = "";
		tagName = "";
		tagDesc = "";
		tagParam = "";
		actualParams.clear();
	}
	
	/**
	 * Name: setPurpose
	 * Description: Sets the purpose 
	 */
	public void setPurpose(String p)
	{
		purpose = p;
	}
	
	/**
	 * Name: addLog
	 * Description: Sets the purpose 
	 */
	public void addLog(String log)
	{
		logs.add(log);
	}

	// Member Accessors and Mutators
	public String getTagType()
	{
		return tagType;
	}

	public String getTagName()
	{
		return tagName;
	}

	public String getTagDesc()
	{
		return tagDesc;
	}

	public String getTagParam()
	{
		return tagParam;
	}

	public String getTagReturn()
	{
		return tagReturn;
	}

	public ArrayList<String> getActualParams()
	{
		return actualParams;
	}

	public String getPurpose()
	{
		return purpose;
	}

	public ArrayList<String> getLogs()
	{
		return logs;
	}
}
