package ccc_ait_util;

import java.awt.*;
import java.awt.datatransfer.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.*;
import java.util.ArrayList;
import javax.imageio.ImageIO;
import javax.swing.*;
import javax.swing.border.Border;
import javax.swing.event.HyperlinkEvent;
import javax.swing.event.HyperlinkListener;
import javax.swing.filechooser.FileNameExtensionFilter;

/**
 * @author jmetcal6
 *	Class for displaying/manipulating graphical elements of the program
 */
@SuppressWarnings("serial")
public class DisplayPane extends JPanel
{
	private final JFileChooser fc; // File chooser menu
	private JPanel menuBtnPanel; // Panel to hold buttons
	private JButton openBtn; // Menu button to open and parse file
	private JButton genBtn; // Button to format and generate documentation from parsed file
	private JButton helpBtn; // Button to show help
	private ImageIcon helpIcon;
	private JButton closeBtn; // Button to close program
	private JPanel editorPanel; // Panel to hold buttons
	private JButton editBtn; // Button to edit formatted file output
	private ImageIcon editIcon;
	private ImageIcon cancelIcon;
	private JButton saveBtn; // Button to save changes over generated documentation file
	private JButton copyBtn; // Button to copy output to clip board
	private JButton resetBtn; // Button to show help
	private boolean editMode; // Flag for whether user is editing or not
	private boolean helping; // Flag for whether user is viewing help or not
	private static JLabel feedback; // Feedback on the current operation
	private JTabbedPane tabbedPane;
	private final String WELCOME_TEXT = "<html><body><center><h1>Welcome to the PL/SQL Documetation Generator.</h1>"
			+ "<br>To help you get started with using the program, please click on the 'Help' button in the menu above.</center></body></html>";
	public static JProgressBar pb;
	private ArrayList<JEditorPane> docPanes; // Editor pane to display HTML formatted text
	private ArrayList<JScrollPane> docScrollers; // Scroll pane for editor pane
	private Help help; // Instance of local inner class to facilitate help menu pop-up
	private static final Border BORDER1 = BorderFactory.createLoweredBevelBorder(); // Lowered bevel border
	private static final Border BORDER2 = BorderFactory.createLineBorder(Color.black); // Lowered line border
	private static final Border BORDER3 = BorderFactory.createRaisedBevelBorder(); // Raised bevel border
	public static final Border RAISED_LINE = BorderFactory.createCompoundBorder(BORDER2, BORDER3); // Compound raised line border
	public static final Border LOWERED_LINE = BorderFactory.createCompoundBorder(BORDER1, BORDER2); // Compound border
	public FileHandler fileHandler; // Class that handles file operations

	/**
	 * Constructor
	 * @throws IOException 
	 */
	public DisplayPane(boolean gui) throws IOException
	{
		fileHandler = new FileHandler(); // Instance of File handler for file operations
		editMode = false; // Flag for whether user is editing or not
		helping = false; // Flag for whether user is viewing help or not

		fc = new JFileChooser(); // File chooser menu
		fc.setDialogTitle("Select a file to open...");
		fc.setMultiSelectionEnabled(true); // Allow multiple file selections 
		// Create filters
		FileNameExtensionFilter htmlFilter = new FileNameExtensionFilter("HTML files (*.html)", "html");
		fc.addChoosableFileFilter(htmlFilter);

		if (gui) // If program is run as GUI
		{
			this.setLayout(null); // No layout for explicit bounds setting
			this.setBackground(new Color(190, 210, 240));

			// The progress bar
			pb = new JProgressBar();
			pb.setValue(0);
			pb.setStringPainted(true);
			pb.setBounds(285, 10, 209, 28);
			pb.setBackground(Color.WHITE);
			pb.setForeground(new Color(30, 160, 70));

			// Open file chooser button
			openBtn = new JButton("Open", new ImageIcon(setUpImage("open.png")));
			openBtn.setToolTipText("Browse to and open source file for parsing");
			openBtn.setBounds(10, 5, 100, 38);
			openBtn.setBorder(RAISED_LINE);
			openBtn.addActionListener(new ActionListener()
			{
				public void actionPerformed(ActionEvent ev)
				{
					hideHelp();
					try
					{
						// Set selected filter
						fc.setFileFilter(null);
						File wd = new File(System.getProperty("user.dir")); // Set the working directory
						fc.setCurrentDirectory(wd);
						int returnVal = fc.showOpenDialog(DisplayPane.this); // Set the location of file chooser
						if (returnVal == JFileChooser.APPROVE_OPTION) // If user selects OK option
						{
							openBtn.setEnabled(false);
							feedback.setText("Please wait while the file is parsed. This may take a few moments...");
							// Facilitates a separate thread for parsing the file as it can take some time. This allows the ability to provide feedback
							SwingWorker<Void, Void> worker = new SwingWorker<Void, Void>()
							{
								// Parse in a separate thread in the background via SwingWorker
								@Override
								protected Void doInBackground()
								{
									fileHandler.loadFile(fc.getSelectedFiles()); // Load the file(s)
									return null;
								}

								// When finished parsing, display the original file, inform user parsing is complete, and enable/disable appropriate buttons
								@Override
								protected void done()
								{
									try
									{
										tabbedPane.removeAll();
										docPanes.clear();
										docScrollers.clear();
										for (int i = 0; i < fileHandler.getSourceFiles().size(); i++)
										{
											tabbedPane.addTab(fileHandler.getSourceFiles().get(i).getSourceName(),
													setUpTab());
											fileHandler.showSourceOutput(docPanes.get(i), i);
											docPanes.get(i).setCaretPosition(0);
										}
									}
									catch (IOException e)
									{
										feedback.setText(
												"<html><font color='red'>Error displaying source file.</font></html>");
										FileHandler
												.logError("ERROR: encountered in display source file exception block. "
														+ e.getMessage());
										e.printStackTrace();
									}
									feedback.setText(fileHandler.getSourceFiles().size()
											+ new String(fileHandler.getSourceFiles().size() > 1 ? " files" : " file")
											+ " successfully loaded and parsed. Ready to generate documentation...");
									genBtn.setEnabled(true);
									resetBtn.setEnabled(true);
									editBtn.setEnabled(false);
								}
							};
							worker.execute(); // Execute the SwingWorker
						}
						else
						{
							feedback.setText("Open a File to get started..."); // Default feedback
						}
					}
					catch (Exception e)
					{
						feedback.setText("<html><font color='red'>Error parsing file. One of the "
								+ new String(fileHandler.getSourceFiles().size() > 1 ? " files" : " file")
								+ " may not be formatted correctly </font></html>");
						FileHandler.logError("ERROR: encountered in open file exception block. " + e.getMessage());
						e.printStackTrace();
						genBtn.setEnabled(false);
					}
				}
			});

			// Generate documentation button
			genBtn = new JButton("Generate Documentation", new ImageIcon(setUpImage("gen.png")));
			genBtn.setToolTipText("Generate and display the documentation produced from the source file(s)");
			genBtn.setBounds(119, 5, 200, 38);
			genBtn.setEnabled(false);
			genBtn.setBorder(RAISED_LINE);
			genBtn.addActionListener(new ActionListener()
			{
				public void actionPerformed(ActionEvent e)
				{
					hideHelp(); // Hide the help in case it is showing
					if (fileHandler.getSourceFiles().size() > 0) // If a file has been loaded
					{
						copyBtn.setEnabled(true);
						fc.setDialogTitle("Specify a location to save the "
								+ new String(fileHandler.getSourceFiles().size() > 1 ? "files " : " file") + "...");
						File wd = new File(System.getProperty("user.dir")); // Set the working directory
						fc.setCurrentDirectory(wd);
						// Set selected filter
						int returnVal = fc.showSaveDialog(DisplayPane.this); // Set the location of file chooser
						if (returnVal == JFileChooser.APPROVE_OPTION) // If user selects OK option
						{
							try
							{
								genBtn.setEnabled(false);
								editBtn.setEnabled(true); // Now we can edit
								editBtn.setText("Edit");
								fileHandler.generateDocumentationFile(true, fc); // Generate the file
								feedback.setText(
										"<html>Documentation file(s) successfully generated and saved. Showing the formatted file output for <font color='green'>" // Feedback
												+ fileHandler.getSourceFiles().size()
												+ new String(
														fileHandler.getSourceFiles().size() > 1 ? " files" : " file")
												+ "</font></html>");
								for (int i = 0; i < docPanes.size(); i++)
								{
									fileHandler.showDocumentation(docPanes.get(i), i); // Display the formatted output
									tabbedPane.setTitleAt(i, fileHandler.getFormattedDocFiles().get(i).getName());
									docPanes.get(i).setCaretPosition(0);
								}
							}
							catch (IOException e1)
							{
								feedback.setText(
										"<html><font color='red'>Error in generating documentation file</font></html>");
								FileHandler.logError("ERROR: encountered in generate documentation exception block. "
										+ e1.getMessage());
							}
						}
					}
					else
					{
						feedback.setText(
								"<html><font color='red'>You have to load a source file before attempting to generate documentation</font></html>"); // If no file has been loaded, feedback
					}
				}
			});

			// Reset everything button
			resetBtn = new JButton("Start Over", new ImageIcon(setUpImage("clear.png")));
			resetBtn.setToolTipText("Resets the entire application, discarding all unsaved changes");
			resetBtn.setBounds(328, 5, 135, 38);
			resetBtn.setBorder(RAISED_LINE);
			resetBtn.setEnabled(false);
			resetBtn.addActionListener(new ActionListener()
			{
				public void actionPerformed(ActionEvent e)
				{
					// Make sure user wants to reset everything
					int reply = JOptionPane.showConfirmDialog(null,
							"Are you sure you want to start over? \nThis will reset EVERYTHING and you will lose any unsaved progress.",
							"Are you sure?", JOptionPane.YES_NO_OPTION);
					if (reply == JOptionPane.YES_OPTION)
					{
						feedback.setText("Open one or more files to get started..."); // Default feedback
						tabbedPane.removeAll();
						docPanes.clear();
						docScrollers.clear();
						tabbedPane.addTab("Welcome!", setUpTab());
						docPanes.get(0).setText(WELCOME_TEXT);
						pb.setValue(0);
						fileHandler.reset();
						hideHelp();
						openBtn.setEnabled(true);
						genBtn.setEnabled(false);
						editBtn.setEnabled(false);
						saveBtn.setEnabled(false);
						copyBtn.setEnabled(false);
						resetBtn.setEnabled(false);
					}
				}
			});

			// Help Panel
			help = new Help();

			// Exit the application button
			helpIcon = new ImageIcon(setUpImage("help.png"));
			helpBtn = new JButton("Help", helpIcon);
			helpBtn.setToolTipText("Click this if you are confused about something!");
			helpBtn.setBounds(472, 5, 100, 38);
			helpBtn.setBorder(RAISED_LINE);
			helpBtn.addActionListener(new ActionListener()
			{
				public void actionPerformed(ActionEvent e)
				{
					if (!helping)
					{
						showHelp();
					}
					else
					{
						hideHelp();
					}

				}
			});

			// Exit the application button
			closeBtn = new JButton("Exit", new ImageIcon(setUpImage("exit.png")));
			closeBtn.setToolTipText("Will close the application and discard any unsaved changes");
			closeBtn.setBounds(580, 5, 95, 38);
			closeBtn.setBorder(RAISED_LINE);
			closeBtn.addActionListener(new ActionListener()
			{
				public void actionPerformed(ActionEvent e)
				{
					System.exit(0);
				}
			});

			// Edit documentation button
			editIcon = new ImageIcon(setUpImage("edit.png")); // Icon for toggled edit/cancel button
			cancelIcon = new ImageIcon(setUpImage("cancel.png")); // Icon for toggled edit/cancel button
			editBtn = new JButton("Edit", editIcon);
			editBtn.setEnabled(false);
			editBtn.setBounds(10, 5, 100, 38);
			editBtn.setBorder(RAISED_LINE);
			editBtn.setToolTipText("Edit the displayed documentation/source code");
			editBtn.addActionListener(new ActionListener()
			{
				public void actionPerformed(ActionEvent e)
				{
					if (editMode) // If we are already editing
					{
						editBtn.setToolTipText("Discard all changes made while editing");

						editBtn.setText("Edit");
						editBtn.setIcon(editIcon);
						try
						{
							// If docOutput ins't null, documentation has been generated
							if (fileHandler.getSourceFiles().size() > 0)
							{
								for (int i = 0; i < docPanes.size(); i++)
								{
									fileHandler.showDocumentation(docPanes.get(i), i); // Display the formatted output
									tabbedPane.setTitleAt(i, fileHandler.getFormattedDocFiles().get(i).getName());
									docPanes.get(i).setCaretPosition(0);
								}
							}
							// Otherwise we have only displayed the original source file
							else
							{
								// Display the original output
								for (int i = 0; i < fileHandler.getSourceFiles().size(); i++)
								{
									fileHandler.showSourceOutput(docPanes.get(i), i);
								}
							}
							tabbedPane.setEnabled(true);
						}
						catch (IOException e1)
						{
							feedback.setText("<html><font color='red'>Error in displaying the file(s)</font></html>");
							FileHandler
									.logError("ERROR: encountered in edit output exception block. " + e1.getMessage());
						}
						saveBtn.setEnabled(false);
						setEditable(false);
						editMode = false;
					}
					else // Else if we are not already editing
					{
						editBtn.setToolTipText("Edit the displayed documentation/source code");
						editBtn.setText("Cancel");
						editBtn.setIcon(cancelIcon);
						saveBtn.setEnabled(true);
						editMode = true;
						setEditable(true);
						tabbedPane.setEnabled(false);
					}
				}
			});

			// Edit documentation button
			saveBtn = new JButton("Save Changes", new ImageIcon(setUpImage("save.png")));
			saveBtn.setToolTipText("Save the changes made while editing the displayed documentation");
			saveBtn.setEnabled(false);
			saveBtn.setBounds(120, 5, 155, 38);
			saveBtn.setBorder(RAISED_LINE);
			saveBtn.addActionListener(new ActionListener()
			{
				public void actionPerformed(ActionEvent e)
				{
					int reply = JOptionPane.showConfirmDialog(null,
							"Are you sure you want to save your changes? \nThis will overwrite the previous documentation file, and this process cannot be undone.",
							"Are you sure?", JOptionPane.YES_NO_OPTION);
					if (reply == JFileChooser.APPROVE_OPTION) // If user selects OK option
					{
						editBtn.setText("Edit");
						editBtn.setIcon(editIcon);
						setEditable(false);
						saveBtn.setEnabled(false);
						copyBtn.setEnabled(true);
						editMode = false;
						tabbedPane.setEnabled(true);
						saveChanges();
						feedback.setText("<html><font color='green'>Changes successfully saved</font></html>");
					}
					else
					{
						feedback.setText("<html>Save canceled</html>");
					}
				}
			});

			// Copy to clip board button
			copyBtn = new JButton("Copy to Clipboard", new ImageIcon(setUpImage("open.png")));
			copyBtn.setToolTipText("Copy the displayed documentation to the clip board");
			copyBtn.setBounds(504, 5, 170, 38);
			copyBtn.setBorder(RAISED_LINE);
			copyBtn.setEnabled(false);
			copyBtn.addActionListener(new ActionListener()
			{
				public void actionPerformed(ActionEvent e)
				{
					Transferable t = new HtmlSelection(docPanes.get(tabbedPane.getSelectedIndex()).getText()); // Create transferable object from inner class
					Toolkit.getDefaultToolkit().getSystemClipboard().setContents(t, null); // Send content to system clip board
					feedback.setText("Contents of displayed documentation file successfully copied to the clip board");
				}
			});

			// Feedback label
			feedback = new JLabel("Open a File to get started...", JLabel.CENTER);
			feedback.setBounds(20, 55, DocGen_Main.WIDTH - 40, 50);

			// Scroll pane for editor/display text pane
			docPanes = new ArrayList<JEditorPane>();
			docScrollers = new ArrayList<JScrollPane>();

			// Tabbed Pane
			tabbedPane = new JTabbedPane();
			tabbedPane.setBackground(new Color(235, 235, 235));
			tabbedPane.setBounds(10, 45, DocGen_Main.WIDTH - 35, 400);
			tabbedPane.addTab("Welcome!", setUpTab());
			docPanes.get(0).setText(WELCOME_TEXT);

			// Menu Button panel
			menuBtnPanel = new JPanel();
			menuBtnPanel.setLayout(null);
			menuBtnPanel.setBounds(5, 10, DocGen_Main.WIDTH - 15, 48);
			menuBtnPanel.setBorder(RAISED_LINE);
			menuBtnPanel.setBackground(new Color(50, 86, 140));
			menuBtnPanel.add(openBtn);
			menuBtnPanel.add(genBtn);
			menuBtnPanel.add(resetBtn);
			menuBtnPanel.add(helpBtn);
			menuBtnPanel.add(closeBtn);

			// Editor Button panel
			editorPanel = new JPanel();
			editorPanel.setLayout(null);
			editorPanel.setBounds(5, 100, DocGen_Main.WIDTH - 15, 460);
			editorPanel.setBorder(RAISED_LINE);
			editorPanel.setBackground(new Color(50, 86, 140));
			editorPanel.add(editBtn);
			editorPanel.add(saveBtn);
			editorPanel.add(pb);
			editorPanel.add(copyBtn);
			editorPanel.add(tabbedPane);

			// Add everything to parent pane
			this.add(menuBtnPanel);
			this.add(help);
			this.add(feedback);
			this.add(editorPanel);
		}
		else
		{
			// !! TODO Reserved for future logic for console only
		}
	}

	/**
	 * Name: setProgress
	 * Description: Updates the progress bar
	 */
	public static void setProgress(int val)
	{
		pb.setValue(val);
	}

	/**
	 * Name: setProgress
	 * Description: Updates the progress bar
	 */
	public static void setProgressMax(int val)
	{
		pb.setMaximum(val);
	}

	/**
	 * Name: setUpPane
	 * Description: Returns a jeditorpane for the jscroller
	 * Return: jeditorpane
	 */
	private JEditorPane setUpPane()
	{
		JEditorPane pane;
		pane = new JEditorPane(); // Editor pane to display HTML formatted text
		pane.setContentType("text/html");
		pane.setDragEnabled(true);
		pane.setEditable(false);
		pane.setBackground(new Color(235, 235, 235));
		pane.setMargin(new Insets(20, 20, 20, 20));
		// Add a hyperlink listener so it can be used right from the application
		pane.addHyperlinkListener(new HyperlinkListener()
		{
			@Override
			public void hyperlinkUpdate(HyperlinkEvent hle)
			{
				if (HyperlinkEvent.EventType.ACTIVATED.equals(hle.getEventType()))
				{
					Desktop desktop = Desktop.getDesktop();
					try
					{
						desktop.browse(hle.getURL().toURI());
					}
					catch (Exception ex)
					{
						feedback.setText(
								"<html><font color='blue'>Page anchor links do not work within the application, only links to external pages. Load the generated HTML page to test the anchor link.</font></html>");
						ex.printStackTrace();
					}
				}
			}
		});
		return pane;
	}

	/**
	 * Name: setUpScroller
	 * Description: Returns a scroll pane to dynamically add a tab for the editor's tabbed pane
	 * Return: tab as JScrollPane 
	 */
	private JScrollPane setUpScroller()
	{
		JScrollPane scroller;
		docPanes.add(setUpPane());
		scroller = new JScrollPane(docPanes.get(docPanes.size() - 1), JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED, // Scroll pane for editor pane
				JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
		scroller.setBounds(10, 45, DocGen_Main.WIDTH - 35, 400);
		scroller.setBorder(LOWERED_LINE);
		scroller.setViewportView(docPanes.get(docPanes.size() - 1));
		return scroller;
	}

	/**
	 * Name: setUpTab
	 * Description: Returns a tab for the editor's tabbed pane
	 * Return: tab from collection as JScrollPane 
	 */
	private JScrollPane setUpTab()
	{
		docScrollers.add(setUpScroller());
		return docScrollers.get(docScrollers.size() - 1);
	}

	/**
	 * Name: showHelp
	 * Description: Shows the help menu
	 */
	private void showHelp()
	{
		helping = true;
		editorPanel.setVisible(false);
		help.setVisible(true);
		helpBtn.setIcon(null);
		helpBtn.setText("Close Help");
	}

	/**
	 * Name: hideHelp
	 * Description: Hides the help menu
	 */
	private void hideHelp()
	{
		helping = false;
		help.setVisible(false);
		editorPanel.setVisible(true);
		helpBtn.setIcon(helpIcon);
		helpBtn.setText("Help");
	}

	/**
	 * Set the text editor pane to editable or not depending on passed argument
	 */
	private void setEditable(Boolean enabled)
	{
		docPanes.get(tabbedPane.getSelectedIndex()).setEditable(enabled);
		if (enabled)
		{
			docPanes.get(tabbedPane.getSelectedIndex()).setBackground(Color.white);
			feedback.setText(
					"<html><font color='blue'>You are in EDIT mode. Click 'Save' to preserve any changes or 'Cancel' to discard any changes</font></html>");
		}
		else
		{
			// Set background to 'grayed out'
			docPanes.get(tabbedPane.getSelectedIndex()).setBackground(new Color(235, 235, 235));
			// If docOutput ins't null, documentation has been generated
			if (fileHandler.getSourceFiles().size() > 0)
			{
				feedback.setText(
						"<html>Documentation file successfully generated and saved. Showing the output for the file(s) <font color='green'></font></html>");
			}
			// Otherwise we have only displayed the original source file
			else
			{
				feedback.setText("File successfully loaded and parsed. Ready to generate documentation file...");
			}
		}
	}

	/**
	 * Name: saveChanges
	 * Description: Saves the changes of the edited documentation file
	 */
	private void saveChanges()
	{
		FileWriter fileWriter;

		// Build the file
		try
		{
			fileWriter = new FileWriter(
					fileHandler.getFormattedDocFiles().get(tabbedPane.getSelectedIndex()).getAbsolutePath());
			fileWriter.write(docPanes.get(tabbedPane.getSelectedIndex()).getText());
			fileWriter.flush();
			fileWriter.close();
		}
		catch (IOException e)
		{
			FileHandler.logError("ERROR: encountered in savechanges exception block. " + e.getMessage());
		}
	}

	/**
	 * 
	 * Provides a static means of setting the feedback label externally from this class
	 */
	public static void setFeedback(String text, String color)
	{
		feedback.setText("<html><font color='" + color + "'>" + text + "</font></html>");
	}

	/**
	 * Assigns the images
	 * @throws IOException 
	 */
	private Image setUpImage(String path) throws IOException
	{
		Image image = ImageIO.read(getClass().getClassLoader().getResource(path));
		return image;
	}

	/**
	 * Inner class that facilitates data transfer operations
	 */
	private class HtmlSelection implements Transferable
	{
		private ArrayList<DataFlavor> htmlFlavors = new ArrayList<DataFlavor>(); // List of htmlFlavors
		private String html; // HTML formatted text

		/**
		 * Constructor
		 */
		public HtmlSelection(String html)
		{
			this.html = html;
			try
			{
				htmlFlavors.add(new DataFlavor("text/html;class=java.lang.String"));
				htmlFlavors.add(new DataFlavor("text/html;class=java.io.Reader"));
			}
			catch (ClassNotFoundException e)
			{
				FileHandler.logError("ERROR: encountered in htmlFlavor exception block. " + e.getMessage());
			}
		}

		/**
		 * Returns an array of DataFlavor objects indicating the flavors the data can be provided in. 
		 * The array should be ordered according to preference for providing the data (from most richly descriptive to least descriptive)
		 */
		public DataFlavor[] getTransferDataFlavors()
		{
			return (DataFlavor[]) htmlFlavors.toArray(new DataFlavor[htmlFlavors.size()]);
		}

		/**
		 *  Returns an object which represents the data to be transferred. The class of the object returned is defined by the representation class of the flavor
		 */
		public Object getTransferData(DataFlavor flavor) throws UnsupportedFlavorException
		{
			if (String.class.equals(flavor.getRepresentationClass()))
			{
				return html;
			}
			else if (Reader.class.equals(flavor.getRepresentationClass()))
			{
				return new StringReader(html);
			}
			throw new UnsupportedFlavorException(flavor);
		}

		/**
		 * Returns whether or not the specified data flavor is supported for this object 
		 */
		public boolean isDataFlavorSupported(DataFlavor arg0)
		{
			return false;
		}
	}
}
