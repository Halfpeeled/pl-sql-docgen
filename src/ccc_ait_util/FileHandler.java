package ccc_ait_util;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.swing.JEditorPane;
import javax.swing.JFileChooser;

/**
 * This class handles all file operations for the documentation generator program
 */
public class FileHandler
{
	private ArrayList<SourceCodeFile> sourceFiles; // The unmodified source code files 
	private ArrayList<DocumentationFile> docFiles; // The Documentation Files as they are prepared from the Parser
	private ArrayList<File> formattedDocFiles; // The HTML formatted Documentation Files from this instance of FileHandler
	private boolean isBody; // Boolean denoting if this is the body or spec
	private static File errorLog; // Error log
	// HTML table tags and styling for building documentation file
	protected final static String TABLE_HEAD = "<table style='width: 80%; border: 1px black solid; background-color: black' width='100%' cellspacing='1' cellpadding='2'>";
	protected final static String TD_HEAD = "<td style='background-color: white'>";
	private String[] tokenizedNewLines; // Tokenized form of lines in the file
	private ParsingHandler parser = new ParsingHandler(); // Instance of parsing class that facilitates parsing, state handling, and formatting
	private StringBuilder builder; // String builder used for reading file into string, then again for the text sans line comments
	private StringBuilder builder2; // String builder used for 
	private boolean ignore; // Boolean flag denoting whether quotes can be ignored because they exist within a block comment
	private boolean singleQuote; // Boolean flag denoting whether single quote is opening or closing 

	/**
	 * Constructor
	 */
	public FileHandler()
	{
		sourceFiles = new ArrayList<SourceCodeFile>();
		docFiles = new ArrayList<DocumentationFile>();
		formattedDocFiles = new ArrayList<File>();
		builder = new StringBuilder();
		builder2 = new StringBuilder();
		isBody = false;
		ignore = false;
		singleQuote = false;
		errorLog = new File("errorLog.txt");
	}

	/**
	 * Name: loadFile 
	 * Description: Loads a specified file via supplied path into
	 * the local file
	 */
	public void loadFile(File[] files)
	{
		DisplayPane.setProgressMax(files.length); // Set the max of the progress bar to the files length
		for (int i = 0; i < files.length; i++)
		{
			sourceFiles.add(new SourceCodeFile(files[i]));
			prepareLoadedFile(files[i], i + 1); // Parse the file
			docFiles.add(parser.getDocFile());
			partialReset();
		}
	}

	/**
	 * Name: showFileOutput Description: Parses the currently loaded file for
	 * anchor tags that signify type of content to follow (Package Headers,
	 * Change logs, Function Headers, etc)
	 * 
	 * @throws IOException
	 */
	private void prepareLoadedFile(File file, int index)
	{
		try
		{
			FileReader fr = new FileReader(file);
			BufferedReader br = new BufferedReader(fr);
			String ln = null;
			String[] tempArry;
			String pkgName = "";

			// Build a string of the file to be searched
			while ((ln = br.readLine()) != null)
			{
				// Determine if the file is either the package body or spec, or stand-alone block, then get the name of the package/block
				if (ln.toUpperCase().matches("\\s*CREATE OR REPLACE PACKAGE BODY\\s+(.*)"))
				{
					isBody = true;
					// Grab everything after the keyword BODY to derive the package name
					pkgName = ln.toUpperCase().substring(ln.toUpperCase().indexOf("BODY") + 4, ln.length())
							.replace("\"", "").trim();
					// Use array to tokenize the line in case the package name did not conclude the line, and trailing words/keyowrods were extracted
					tempArry = pkgName.split("\\s+");
					// The first element is everything before the first space, so it is all we care about
					pkgName = tempArry[0];
					parser.setSourceFileName(pkgName);
				}
				else if (ln.toUpperCase().matches("\\s*CREATE OR REPLACE PACKAGE\\s+(.*)"))
				{
					isBody = false;
					// Grab everything after the keyword BODY to derive the package name
					pkgName = ln.toUpperCase().substring(ln.toUpperCase().indexOf("PACKAGE") + 7, ln.length())
							.replace("\"", "").trim();
					// Use array to tokenize the line in case the package name did not conclude the line, and trailing words/keyowrods were extracted
					tempArry = pkgName.split("\\s+");
					// The first element is everything before the first space, so it is all we care about
					pkgName = tempArry[0];
					parser.setSourceFileName(pkgName);
				}
				builder.append(ln.toString() + "\n"); // Append each line of the file to the string builder to ease parsing / manipulation
			}
			br.close(); // Close the buffered reader

			extractLineComments(); // Remove line comments
			extractRemainingStringLiterals(); // Escape keywords found within strings
			DisplayPane.setFeedback(
					"Line comments and string literals escaped for file " + file.getName() + ". Parsing...", null);
			DisplayPane.setProgress(index - Math.round((index / 2)));

			// Start recursively parsing the "cleaned" tokens
			parser.initStateChecking(builder2.toString(), 0, isBody);
			DisplayPane.setFeedback("Finished parsing " + file.getName(), null);
			DisplayPane.setProgress(index);

		}
		catch (IOException e)
		{
			logError("ERROR: encountered in prepareLoadedFile exception block " + e.getMessage());
		}
	}

	/**
	 * Name: extractLineComments
	 * Description: Parses the file as a string for line comments and extracts them to avoid improper detection of keywords later.
	 * Parameters: the loaded file as a String
	 * Return: The file sans line comments as a String
	 */
	private void extractLineComments()
	{
		tokenizedNewLines = builder.toString().split("\n"); // Tokenize the string by splitting on newlines
		builder.setLength(0); // Reset string builder
		builder.trimToSize(); // Empty buffer
		String[] lineComArry; // Temporary array container for tokens split on line comment characters
		Pattern p1 = Pattern.compile("\\'[^\\'\\r\\n]*\\'"); // Everything within single quotes on same line
		Pattern p2 = Pattern.compile("/\\*.*\\*/"); // Everything within a Block Comment on same line
		Matcher m1;
		Matcher m2;
		String temp = ""; // Temporary container
		String temp2 = ""; // Temporary container

		// Start by removing any line comment characters '--' that reside within quotes to prevent improper extraction
		for (int i = 0; i < tokenizedNewLines.length; i++)
		{
			m1 = p1.matcher(tokenizedNewLines[i]);
			m2 = p2.matcher(tokenizedNewLines[i]);

			// While loops used to accommodate multiple instances of enclosed strings
			while (m1.find()) // While token has an enclosed string, check for line comment characters
			{
				if (m1.group().contains("--")) // If the matched group contains line comment characters
				{
					temp = m1.group().replaceAll("--", ""); // Replace all line comment characters with empty strings and assign to temporary container  
					tokenizedNewLines[i] = tokenizedNewLines[i].replaceAll("\\Q" + m1.group().toString() + "\\E", temp); // Then assign each token with itself, replacing the part containing the line comment characters 
				}
			}
			while (m2.find()) // While the token has single line block comment, check for line comment characters
			{
				if (m2.group().contains("--")) // If the matched group contains line comment characters
				{
					temp2 = m2.group().replaceAll("--", ""); // Replace all line comment characters with empty strings and assign to temporary container  
					tokenizedNewLines[i] = tokenizedNewLines[i].replaceAll("\\Q" + m2.group().toString() + "\\E",
							temp2); // Then assign each token with itself, replacing the part containing the line comment characters 
				}
			}
		}

		// Now iterate through tokenized lines and separate line comments from everything else
		for (int i = 0; i < tokenizedNewLines.length; i++)
		{
			// Every iteration split on line comment characters
			lineComArry = tokenizedNewLines[i].split(parser.getKeywords().get("LINE_COMMENT_START"));
			// If array has more than one element, that means there may be text that needs preserved to the left of the line comment (ex. <some code to the left> -- This is the line comment accompanying code
			if (lineComArry.length > 1)
			{
				builder.append(lineComArry[0] + " "); // First element isn't a line comment, so append to the string builder
			}
			else // Otherwise it's everything else that needs parsing later
			{
				builder.append(tokenizedNewLines[i] + " ");
			}
		}
	}

	/**
	 * Name: extractRemainingStringLiterals
	 * Description: Removes the remaining strings that carried over more than one line and thus were not picked up before
	 */
	private void extractRemainingStringLiterals()
	{
		for (int i = 0; i < builder.toString().length(); i++)
		{
			// Ignore single quotes found in block comments
			if (builder.toString().charAt(i) == '/' && builder.toString().charAt(i + 1) == '*')
			{
				ignore = true;
			}
			else if (builder.toString().charAt(i) == '*' && builder.toString().charAt(i + 1) == '/')
			{
				ignore = false;
			}

			// If not already singlequote, this is the opening quote
			if (builder.toString().charAt(i) == '\'' && !singleQuote && !ignore)
			{
				singleQuote = true;
			}
			// Else, if it's a single quote and already singlequote, it's closing
			else if (builder.toString().charAt(i) == '\'' && singleQuote && !ignore)
			{
				singleQuote = false;
			}
			// Else if any other character and not within a single quote, append to the stirng builder of text to keep
			else if (!singleQuote)
			{
				builder2.append(builder.toString().charAt(i));
			}
		}
	}

	/**
	 * Name: fileExists
	 * Description: Checks the passed directory for the existence of the passed file
	 * Parameters: File to be checked for as File, Directory to check as String
	 * Return: The count of files as an integer
	 */
	public int fileExists(File f, File dir)
	{
		File[] listOfFiles = dir.listFiles(); // List of files already in folder
		int fileCount = 0;
		String files = "";
		String fName = "";
		for (int i = 0; i < listOfFiles.length; i++)
		{
			// Count existing files with same name
			if (listOfFiles[i].isFile())
			{
				// Successively get each file minus extension in the directory with every iteration 
				files = listOfFiles[i].getName().toLowerCase().substring(0,
						listOfFiles[i].getName().toLowerCase().indexOf("."));
				fName = f.getName().toLowerCase(); // The name of the file
				// If the selected file name matches any files with or without any affixed numbers
				if (files.matches(fName + "\\d*$"))
				{
					fileCount++; // Add to the file count
				}
			}
		}
		return fileCount;
	}

	/**
	 * Name: generateDocumentation
	 * Description: Writes to and formats the documentation file
	 * Parameters: Boolean flagging whether program was run as gui or from cli, FileChooser to get user selected file/directory
	 */
	public void generateDocumentationFile(boolean gui, JFileChooser fc)
	{
		File[] files = new File[docFiles.size()];
		// Determine if the name should be modified with the count of the potential duplicates
		String nameMod = "";

		for (int i = 0; i < docFiles.size(); i++)
		{
			nameMod = (fileExists(new File(docFiles.get(i).getName().toLowerCase() + "_doc"),
					fc.getCurrentDirectory()) > 0)
							? Integer.toString(fileExists(new File(docFiles.get(i).getName().toLowerCase() + "_doc"),
									fc.getCurrentDirectory()))
							: "";
			files[i] = new File(
					fc.getCurrentDirectory() + "/" + docFiles.get(i).getName() + "_DOC" + nameMod + ".html");
			// Format and write the documentation file
			//matchPackageSpecAndBody();
			formattedDocFiles.add(files[i]);
			writeDocumentation(formattedDocFiles.get(i), i);
		}
	}

	private void matchPackageSpecAndBody()
	{
		for (int i = 0; i < docFiles.size(); i++)
		{
			for (int j = 0; j < docFiles.size(); j++)
			{
				if (!docFiles.get(i).getIsBody() && docFiles.get(i).getName().matches(docFiles.get(j).getName()))
				{
					System.out.println("MATCH: " + docFiles.get(i).getName() + " | " + docFiles.get(j).getName());
				}
			}
		}
	}

	/**
	 * Name: writeDocumentation
	 * Description: Writes to and formats the documentation file
	 */
	public void writeDocumentation(File docFile, int index)
	{
		// Build the file
		try
		{
			FileWriter fw;
			fw = new FileWriter(docFile);
			fw.write("<!DOCTYPE html><html><head><meta charset='UTF-8'><title>" + docFiles.get(index).getName()
					+ " Documentation</title></head><body><center>");

			fw.write("<div><center><b><h1>" + docFiles.get(index).getName() + " Package "
					+ new String(docFiles.get(index).getIsBody() ? "Body" : "Spec") + "</h1></b></center>");

			// Package Summary
			fw.write("<center><b><h2>Package Summary</h2></b></center>");
			fw.write(TABLE_HEAD + "<tr>" + TD_HEAD + "This package has " + docFiles.get(index).getVariables().size()
					+ " package level variables, "
					+ (docFiles.get(index).getCompleteDecBlocks().size()
							+ docFiles.get(index).getOrphanDecBlocks().size())
					+ " function and procedure declarations, and "
					+ (docFiles.get(index).getCompleteBlocks().size() + docFiles.get(index).getOrphanBlocks().size())
					+ " function and procedure definitions.</td></tr></table><br>");

			// Package Outline
			fw.write("<center><b><h2>Package Outline</h2></b></center>");
			fw.write(TABLE_HEAD + "<tr>" + TD_HEAD + docFiles.get(index).getOutline() + "</td></tr></table><br>");

			// Package errors
			fw.write("<center><b><h2>Package Issues</h2></b></center>");
			fw.write(TABLE_HEAD + "<tr>" + TD_HEAD + "<b>This package has the following error(s):</b><br>"
					+ (docFiles.get(index).getChangelog().isEmpty() ? "No Changelog Found, or Improperly Formatted<br>"
							: "")
					+ (docFiles.get(index).getPurpose().isEmpty()
							? "No Package Purpose Found, or Improperly Formatted<br>" : "")
					+ new String(docFiles.get(index).getOrphanDecBlocks().size() > 0
							? docFiles.get(index).getOrphanDecBlocks().size()
									+ " function and procedure declarations without properly formatted headers.<br>"
							: "")
					+ new String(
							docFiles.get(index).getOrphanBlocks().size() > 0
									? docFiles.get(index).getOrphanBlocks().size()
											+ " function and procedure definitions without properly formatted headers."
									: "")
					+ (docFiles.get(index).getPackageEndError() ? "The package does not end with its identifier." : "")
					+ "</td></tr></table><br>");

			// Package Purpose
			if (!docFiles.get(index).getPurpose().isEmpty())
			{
				fw.write("<center><b><h2>Package Purpose</h2></b></center>");
				fw.write(TABLE_HEAD + "<tr>" + TD_HEAD + docFiles.get(index).getPurpose() + "</td></tr></table><br>");
			}

			// Changelog
			fw.write(docFiles.get(index).getChangelog());

			// If their are package level variable tags
			if (docFiles.get(index).getVariables().size() > 0)
			{
				fw.write("<center><b><h2>Package Level Variables</h2></b></center>");
				for (int i = 0; i < docFiles.get(index).getVariables().size(); i++)
				{
					fw.write(TABLE_HEAD + "<tr>" + TD_HEAD + docFiles.get(index).getVariables().get(i)
							+ "</td></tr></table><br>");
				}
			}

			// Function and procedure declarations with headers
			if (docFiles.get(index).getCompleteDecBlocks().size() > 0)
			{
				fw.write("<center><b><h2>Functions and Procedure Declarations</h2></b></center>");
				for (int k = 0; k < docFiles.get(index).getCompleteDecBlocks().size(); k++)
				{
					// Format the anchor tags
					fw.write("<br><div id='"
							+ docFiles.get(index).getCompleteDecBlocks().get(k).getTag().getTagName().toLowerCase()
									.trim()
							+ "'>" + TABLE_HEAD + "<tr>" + TD_HEAD + "<center><b>"
							+ docFiles.get(index).getCompleteDecBlocks().get(k).getTag().getTagType().substring(0, 1)
							+ docFiles.get(index).getCompleteDecBlocks().get(k).getTag().getTagType().substring(1)
									.toLowerCase()
							+ " Header Details </b></center></td></tr>");
					fw.write("<tr>" + TD_HEAD + "<b>Type:</b> "
							+ docFiles.get(index).getCompleteDecBlocks().get(k).getTag().getTagType()
							+ "</b></td></tr>");
					fw.write("<tr>" + TD_HEAD + "<b>Name:</b> "
							+ docFiles.get(index).getCompleteDecBlocks().get(k).getTag().getTagName() + "</td></tr>");
					fw.write("<tr>" + TD_HEAD + "<b>Description:</b> "
							+ docFiles.get(index).getCompleteDecBlocks().get(k).getTag().getTagDesc() + "</td></tr>");
					fw.write("<tr>" + TD_HEAD + "<b>Parameter(s)</b> "
							+ docFiles.get(index).getCompleteDecBlocks().get(k).getTag().getTagParam() + "</td></tr>");
					if (docFiles.get(index).getCompleteDecBlocks().get(k).getTag().getTagReturn() != null)// If there is a return tag
					{
						fw.write("<tr>" + TD_HEAD + "<b>Return:</b> "
								+ docFiles.get(index).getCompleteDecBlocks().get(k).getTag().getTagReturn()
								+ "</td></tr>");
					}

					// Format the data found within source code that had preceding anchor tags
					fw.write("<tr>" + TD_HEAD + "<center><b>Source Code Details</b></center></td></tr>");
					fw.write("<tr>" + TD_HEAD + "<b>Actual Type: </b>"
							+ docFiles.get(index).getCompleteDecBlocks().get(k).getBlock().getType() + "</td></tr>");
					fw.write("<tr>" + TD_HEAD + "<b>Actual Name: </b>"
							+ docFiles.get(index).getCompleteDecBlocks().get(k).getBlock().getName() + "</td></tr>");
					fw.write("<tr>" + TD_HEAD + "<b>Actual Parameter(s) Within Source Code</b></td></tr>");
					for (int j = 0; j < docFiles.get(index).getCompleteDecBlocks().get(k).getBlock()
							.getParams().length; j++)
					{
						fw.write("<tr>" + TD_HEAD
								+ docFiles.get(index).getCompleteDecBlocks().get(k).getBlock().getParams()[j]
								+ "</td></tr>");
					}
					// If this is a function, write the return type to the file
					if (docFiles.get(index).getCompleteDecBlocks().get(k).getBlock().getType()
							.equalsIgnoreCase("FUNCTION"))
					{
						fw.write("<tr>" + TD_HEAD + "<b>Actual Return Type: </b>"
								+ docFiles.get(index).getCompleteDecBlocks().get(k).getBlock().getReturnType()
								+ "</td></tr>");
					}
					fw.write("</table></div>");
				}
			}

			// If there are orphaned declaration blocks without headers
			if (docFiles.get(index).getOrphanDecBlocks().size() > 0)
			{
				// Build orphaned code blocks that had no preceding anchor tags
				fw.write(
						"<center><b><h2>Functions and Procedures Declarations Without Headers or Descriptions</h2></b></center>");
				for (int c = 0; c < docFiles.get(index).getOrphanDecBlocks().size(); c++)
				{
					fw.write("<br><div id='"
							+ docFiles.get(index).getOrphanDecBlocks().get(c).getName().toLowerCase().trim() + "'>"
							+ TABLE_HEAD + "<tr>" + TD_HEAD + "<b>Type: </b>"
							+ docFiles.get(index).getOrphanDecBlocks().get(c).getType() + "</td></tr>");
					fw.write("<tr>" + TD_HEAD + "<b>Name: </b>"
							+ docFiles.get(index).getOrphanDecBlocks().get(c).getName() + "</td></tr>");
					// Format the parameters found within source code that had NO preceding anchor tags
					fw.write("<tr>" + TD_HEAD + "<b>Parameter(s):</b><br>");
					for (int n = 0; n < docFiles.get(index).getOrphanDecBlocks().get(c).getParams().length; n++)
					{
						fw.write(docFiles.get(index).getOrphanDecBlocks().get(c).getParams()[n] + "<br>");
					}
					fw.write("</td></tr>");
					// If this is a function, write the return type to the file
					if (docFiles.get(index).getOrphanDecBlocks().get(c).getType().equalsIgnoreCase("FUNCTION"))
					{
						fw.write("<tr>" + TD_HEAD + "<b>Return: </b>"
								+ docFiles.get(index).getOrphanDecBlocks().get(c).getReturnType() + "</td></tr>");
					}
					fw.write("</table></div>");
				}
			}

			// If there are complete blocks with tags
			if (docFiles.get(index).getCompleteBlocks().size() > 0)
			{
				fw.write("<center><b><h2>Functions and Procedure Definitions</h2></b></center>");
				for (int m = 0; m < docFiles.get(index).getCompleteBlocks().size(); m++)
				{
					// Format the anchor tags
					fw.write("<br><div id='"
							+ docFiles.get(index).getCompleteBlocks().get(m).getTag().getTagName().toLowerCase().trim()
							+ "'>" + TABLE_HEAD + "<tr>" + TD_HEAD + "<center><b>"
							+ docFiles.get(index).getCompleteBlocks().get(m).getTag().getTagType().substring(0, 1)
							+ docFiles.get(index).getCompleteBlocks().get(m).getTag().getTagType().substring(1)
									.toLowerCase()
							+ " Header Details </b></center></td></tr>");
					fw.write("<tr>" + TD_HEAD + "<b>Type:</b> "
							+ docFiles.get(index).getCompleteBlocks().get(m).getTag().getTagType() + "</b></td></tr>");
					fw.write("<tr>" + TD_HEAD + "<b>Name:</b> "
							+ docFiles.get(index).getCompleteBlocks().get(m).getTag().getTagName() + "</td></tr>");
					fw.write("<tr>" + TD_HEAD + "<b>Description:</b> "
							+ docFiles.get(index).getCompleteBlocks().get(m).getTag().getTagDesc() + "</td></tr>");
					fw.write("<tr>" + TD_HEAD + "<b>Parameter(s)</b> "
							+ docFiles.get(index).getCompleteBlocks().get(m).getTag().getTagParam() + "</td></tr>");
					if (docFiles.get(index).getCompleteBlocks().get(m).getTag().getTagReturn() != null)// If there is a return tag
					{
						fw.write("<tr>" + TD_HEAD + "<b>Return:</b> "
								+ docFiles.get(index).getCompleteBlocks().get(m).getTag().getTagReturn()
								+ "</td></tr>");
					}

					// Format the data found within source code that had preceding anchor tags
					fw.write("<tr>" + TD_HEAD + "<center><b>Source Code Details</b></center></td></tr>");
					fw.write("<tr>" + TD_HEAD + "<b>Actual Type: </b>"
							+ docFiles.get(index).getCompleteBlocks().get(m).getBlock().getType() + "</td></tr>");
					fw.write("<tr>" + TD_HEAD + "<b>Actual Name: </b>"
							+ docFiles.get(index).getCompleteBlocks().get(m).getBlock().getName() + "</td></tr>");
					fw.write("<tr>" + TD_HEAD + "<b>Actual Parameter(s) Within Source Code</b></td></tr>");
					for (int l = 0; l < docFiles.get(index).getCompleteBlocks().get(m).getBlock()
							.getParams().length; l++)
					{
						fw.write("<tr>" + TD_HEAD
								+ docFiles.get(index).getCompleteBlocks().get(m).getBlock().getParams()[l]
								+ "</td></tr>");
					}
					// If this is a function, write the return type to the file
					if (docFiles.get(index).getCompleteBlocks().get(m).getBlock().getType()
							.equalsIgnoreCase("FUNCTION"))
					{
						fw.write("<tr>" + TD_HEAD + "<b>Actual Return Type: </b>"
								+ docFiles.get(index).getCompleteBlocks().get(m).getBlock().getReturnType()
								+ "</td></tr>");
					}
					fw.write("</table></div>");
				}
			}

			// If there are orphaned blocks without headers
			if (docFiles.get(index).getOrphanBlocks().size() > 0)
			{
				// Build orphaned code blocks that had no preceding anchor tags
				fw.write("<center><b><h2>Functions and Procedures Without Headers or Descriptions</h2></b></center>");
				for (int h = 0; h < docFiles.get(index).getOrphanBlocks().size(); h++)
				{
					fw.write("<br><div id='"
							+ docFiles.get(index).getOrphanBlocks().get(h).getName().toLowerCase().trim() + "'>"
							+ TABLE_HEAD + "<tr>" + TD_HEAD + "<b>Type: </b>"
							+ docFiles.get(index).getOrphanBlocks().get(h).getType() + "</td></tr>");
					fw.write("<tr>" + TD_HEAD + "<b>Name: </b>" + docFiles.get(index).getOrphanBlocks().get(h).getName()
							+ "</td></tr>");
					// Format the parameters found within source code that had NO preceding anchor tags
					fw.write("<tr>" + TD_HEAD + "<b>Parameter(s):</b><br>");
					for (int e = 0; e < docFiles.get(index).getOrphanBlocks().get(h).getParams().length; e++)
					{
						fw.write(docFiles.get(index).getOrphanBlocks().get(h).getParams()[e] + "<br>");
					}
					fw.write("</td></tr>");
					// If this is a function, write the return type to the file
					if (docFiles.get(index).getOrphanBlocks().get(h).getType().equalsIgnoreCase("FUNCTION"))
					{
						fw.write("<tr>" + TD_HEAD + "<b>Return: </b>"
								+ docFiles.get(index).getOrphanBlocks().get(h).getReturnType() + "</td></tr>");
					}
					fw.write("</table></div>");
				}
			}
			fw.write("</div></center></body></html>");
			fw.flush();
			fw.close();
		}
		catch (IOException e)
		{
			logError("ERROR encountered in generate documentation file exception block" + e.getMessage());
			e.printStackTrace();
		}
	}

	/**
	 * Name: showSourceOutput 
	 * Description: Displays currently loaded source file, removing all HTML tags to prevent errors with Editor Pane HTML formatting
	 */
	public void showSourceOutput(JEditorPane jte, int index) throws IOException
	{
		String ln = null;
		String doc = "";

		FileReader fr = new FileReader(sourceFiles.get(index).getFile());
		BufferedReader br = new BufferedReader(fr);

		while ((ln = br.readLine()) != null)
		{
			doc += ln.toString().replaceAll("(\\<.*?\\>)", "<i>*HTML TAG REMOVED*</i>") + "<br>";
		}
		br.close();
		jte.setText(doc);
	}

	/**
	 * Name: showDocumentation 
	 * Description: Prints the currently loaded file to the
	 * console
	 */
	public void showDocumentation(JEditorPane jte, int index) throws IOException
	{
		String ln = null;
		String doc = "";

		for (int i = 0; i < formattedDocFiles.size(); i++)
		{
			FileReader fr = new FileReader(formattedDocFiles.get(index));
			BufferedReader br = new BufferedReader(fr);

			while ((ln = br.readLine()) != null)
			{
				doc += ln.toString();
			}
			br.close();
		}
		jte.setText(doc);
	}

	public ArrayList<SourceCodeFile> getSourceFiles()
	{
		return sourceFiles;
	}

	/**
	 * Name: getSourceFileName
	 * Description: Returns the source filename via the parser object method 
	 */
	public String getSourceFileName()
	{
		return parser.getSourceFileName();
	}

	public ArrayList<DocumentationFile> getDocFiles()
	{
		return docFiles;
	}

	public ArrayList<File> getFormattedDocFiles()
	{
		return formattedDocFiles;
	}

	/**
	 * Name: logError
	 * Description: Writes an error message to the local log file
	 * Parameters: The error message as a string
	 * Return: void
	 */
	public static void logError(String errMsg)
	{
		FileWriter fw;
		try
		{
			fw = new FileWriter(errorLog, true);
			// Write time stamp and error to error log file
			fw.write(new java.text.SimpleDateFormat("MM/dd/yyyy h:mm:ss a").format(new Date()) + " - " + errMsg
					+ System.getProperty("line.separator"));
			fw.flush();
			fw.close();
		}
		catch (IOException e)
		{
			System.out.println("ERROR: ");
			e.printStackTrace();
			System.out.println("encountered in error log creation exception block. Obviously we can't log this...");
		}
	}

	/**
	 * Name: partialReset
	 * Description: Resets some of the global variables to facilitate refreshing to parse the next file
	 */
	public void partialReset()
	{
		builder.setLength(0); // Reset string builder
		builder.trimToSize(); // Empty buffer
		builder2.setLength(0); // Reset string builder
		builder2.trimToSize(); // Empty buffer
		tokenizedNewLines = new String[0];
		isBody = false;
		ignore = false;
		singleQuote = false;
	}

	/**
	 * Name: reset
	 * Description: Resets all of the global variables
	 */
	public void reset()
	{
		partialReset();
		formattedDocFiles.clear();
		docFiles.clear();
		sourceFiles.clear();
		parser.reset();
	}

}