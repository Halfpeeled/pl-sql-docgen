# README #

This Java desktop application is used to generate documentation for PL/SQL packages and procedures. It uses annotations (anchor tags) similar to javadocs, though it can generate gneral information even if none are provided.

This project has since gone dormant, but the eventual plans were to refactor it as a sqldeveloper plugin, and give it the ability to automatically push documentation to Confluence, or other documentation sites.